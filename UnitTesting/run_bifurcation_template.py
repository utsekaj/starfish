import sys
import os
import gc
import logging
import matplotlib.pyplot as plt
import h5py
formatter = logging.Formatter('%(name)s - %(lineno)d -  %(levelname)s - %(message)s')
logger = logging.getLogger('starfish')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("test_templates.log", mode="w") #TODO better formatting
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
stderr = logging.StreamHandler()
stderr.setLevel(logging.DEBUG)
stderr.setFormatter(formatter)
logger.addHandler(stderr)
cur = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from template_runner import run_simulation, plot_results

if __name__ == "__main__":
    network_template_path = "../starfish/TemplateNetworks/singleBifurcation_template/singleBifurcation_template.xml"

    #network_template_path = "../starfish/TemplateNetworks/singleVessel_template/singleVessel_template.xml"

    #network_template_path = "../starfish/TemplateNetworks/singleVessel_template/singleVessel_template_influx.xml"

    dt=-2
    run_simulation(network_template_path, method='ConstantPressure', dt=dt)
    run_simulation(network_template_path, method='MeanPressure')
    run_simulation(network_template_path, method='MeanFlow')
    run_simulation(network_template_path, method='AutoLinearSystem', dt=dt)
    plot_results(network_template_path, method='ConstantPressure', fmtstr='-', dt=dt)
    plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^',  dt=dt, alpha=0.25)
    plt.show()
    # dt=-1
    # run_simulation(network_template_path, method='ConstantPressure', dt=dt)
    # run_simulation(network_template_path, method='MeanPressure', dt=dt)
    # run_simulation(network_template_path, method='MeanFlow', dt=dt)
    # run_simulation(network_template_path, method='Auto', dt=dt)
    # run_simulation(network_template_path, method='AutoLinearSystem')
    # plot_results(network_template_path, method='ConstantPressure', fmtstr='-', dt=dt)
    # plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    # plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    # plot_results(network_template_path, method='Auto', fmtstr=':o', alpha=0.25, dt=dt)
    # plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^',  alpha=0.25)
    plt.show()
