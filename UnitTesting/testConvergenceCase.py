'''
Created on Sep 28, 2016

@author: fredrik

To run convergence check and running networkReduction:


#### make sure the starfish WD is set #####

#### run convergence check (better to test with AoBif due to CPU times) ####

cd $pathToStarfishSRC
python testConvergeceCase.py $pathToConvergenceXMLFile # "networkName" in pathToConvergenceXMLFile should be placed in starfish WD

# files will be created in starfish WD


#### run a series of reduced networks (files in Full96Model) ####

# create a pickle file with info for running a series of network reduction cases

cd Full96Model python makebatchList.py

# update tag "batchDataFile" in NetworkRedutionXMLFile

cd $pathToStarfishSRC

python make_arg.py $pathToNetworkRedutionXMLFile # "networkName" in pathToConvergenceXMLFile should be placed in starfish WD

# files will be created in starfish WD

'''

import starfish.NetworkTests.classConvergenceCase as cConvergenceCase
convergenceXmlCaseFile = 'convergenceCaseFromGeomFile.xml'
cCCase = cConvergenceCase.ConvergenceCase()
# cCCase.loadXMLFile(convergenceXmlCaseFile)
# cCCase.createEvaluationCaseFiles(convergenceXmlCaseFile.replace('.xml', '.p'))

convergenceXMLCaseFile = 'testConvergence.xml'
cCCase = cConvergenceCase.ConvergenceCase()
cCCase.networkName = 'Full96Model'
cCCase.createNetworks = True
cCCase.solveNetworks = True
cCCase.processNetworks = True
cCCase.multiprocessing = False
cCCase.numberOfProcessors = 1
cCCase.numberOfRefinements = 4
cCCase.vesselOfInterest = 1
cCCase.dataType = 'P'
cCCase.writeXMLFile(convergenceXMLCaseFile)

# cCCase.createEvaluationCaseFiles(convergenceXmlCaseFile.replace('.xml', '.p'))
