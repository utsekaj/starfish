import os
import sys
import matplotlib.pyplot as plt
import h5py

cur = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
method='Auto'
vessels_to_compare = [0, 1, 5, 43, 16, 70]

networkName = os.path.basename(network_template_path)

pathSolutionDataFilename = os.path.join(cur, networkName + "_" + method + "_tmp_sol.hdf5")
print(pathSolutionDataFilename)
solutionNew = h5py.File(pathSolutionDataFilename, "r")
pathSolutionDataFilename = os.path.join(cur, '..', '..', 'starfish_comp', 'UnitTesting',
                                        networkName + "_" + method + "_tmp_sol.hdf5")
pathSolutionDataFilename = '/home/jsturdy/Biomech/starfish_comp/UnitTesting/adan56_benchmark2015_template.xml_Auto_tmp_sol_preNredAdaptedGrid.hdf5'
print(pathSolutionDataFilename)
solutionOld = h5py.File(pathSolutionDataFilename, "r")
print("Current", "Old")
netNew = solutionNew['VascularNetwork']
netOld = solutionOld['VascularNetwork']
#print(netNew['dt'], netOld['dt'])
#print(netNew['nTSteps'], netOld['nTSteps'])
print(netNew['simulationTime'][0:2], netOld['simulationTime'][0:2])

tNew = netNew['simulationTime']
tOld = netOld['simulationTime']
vesselsNew = solutionNew['vessels']
vesselsOld = solutionOld['vessels']
nStepsCompare = 5000
for subGroupName, subGroup in vesselsNew.items():
    vesselId = int(subGroupName.split(' - ')[-1])
    if vesselId == 1:
        Qnew = subGroup['Qsol']
        Qold = vesselsOld[subGroupName]['Qsol']
        fig, ax = plt.subplots(1, 1, num='flow')  # number=subGroupName)q
        ax.plot(tNew, Qnew[:,0], label='New')
        ax.plot(tOld, Qold[:,0], ':', label='Old', alpha=0.5)

    if vesselId in vessels_to_compare:
        Pnew = subGroup['Psol']
        Pold = vesselsOld[subGroupName]['Psol']
        Qnew = subGroup['Qsol']
        Qold = vesselsOld[subGroupName]['Qsol']
        fig, ax = plt.subplots(2,1, num=subGroupName)
        ax[0].plot(tNew[0:nStepsCompare], Pnew[0:nStepsCompare, 6], label='New')
        ax[0].plot(tOld[0:nStepsCompare], Pold[0:nStepsCompare, 6], ':', label='Old', alpha=0.5)
        ax[1].plot(tNew[0:nStepsCompare], Qnew[0:nStepsCompare, 6], label='New')
        ax[1].plot(tOld[0:nStepsCompare], Qold[0:nStepsCompare, 6], ':', label='Old', alpha=0.5)

        plt.legend()
plt.show()