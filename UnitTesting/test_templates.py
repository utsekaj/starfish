import sys
import os
import logging
import matplotlib.pyplot as plt

formatter = logging.Formatter('%(name)s - %(lineno)d -  %(levelname)s - %(message)s')
logger = logging.getLogger('starfish')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("test_templates.log", mode="w") #TODO better formatting
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
stderr = logging.StreamHandler()
stderr.setLevel(logging.DEBUG)
stderr.setFormatter(formatter)
logger.addHandler(stderr)
cur = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from template_runner import run_simulation, plot_results
totalTime = 0.1

def test_adan_const_press_dt1():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt=-1
    run_simulation(network_template_path, method='ConstantPressure', dt=dt, totalTime=totalTime)

def test_adan_mean_press_dt1():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -1
    run_simulation(network_template_path, method='MeanPressure', dt=dt, totalTime=totalTime)

def test_adan_mean_flow_dt1():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -1
    run_simulation(network_template_path, method='MeanFlow', dt=dt, totalTime=totalTime)

def test_adan_auto_dt1():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -1
    run_simulation(network_template_path, method='Auto', dt=dt, totalTime=totalTime)


def test_adan_auto_linear_system_dt1():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -1
    run_simulation(network_template_path, method='AutoLinearSystem', dt=dt, totalTime=totalTime)

def test_adan_const_press_dt2():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt=-2
    run_simulation(network_template_path, method='ConstantPressure', dt=dt, totalTime=totalTime)

def test_adan_mean_press_dt2():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -2
    run_simulation(network_template_path, method='MeanPressure', dt=dt, totalTime=totalTime)


def test_adan_mean_flow_dt2():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -2
    run_simulation(network_template_path, method='MeanFlow', dt=dt, totalTime=totalTime)

def test_adan_auto_dt2():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -2
    run_simulation(network_template_path, method='Auto', dt=dt, totalTime=totalTime)

def test_adan_auto_linear_system_dt2():
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"
    dt = -2
    run_simulation(network_template_path, method='AutoLinearSystem', dt=dt, totalTime=totalTime)


if __name__ == "__main__":
    network_template_path = "../starfish/TemplateNetworks/adan56_benchmark2015_template/adan56_benchmark2015_template.xml"

    dt=-2
    run_simulation(network_template_path, method='ConstantPressure', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='MeanPressure', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='MeanFlow', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='Auto', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='AutoLinearSystem', dt=dt, totalTime=totalTime)
    plot_results(network_template_path, method='ConstantPressure', fmtstr='-', dt=dt)
    plot_results(network_template_path, method='MeanPressure', fmtstr='--', dt=dt, alpha=0.45)
    plot_results(network_template_path, method='MeanFlow', fmtstr='.-', dt=dt, alpha=0.35)
    #plot_results(network_template_path, method='Auto', fmtstr=':o', dt=dt, alpha=0.25)
    #plot_results(network_template_path, method='AutoLinearSystem', dt=dt, fmtstr=':^',  alpha=0.25)
    #plt.show()
    dt=-1
    run_simulation(network_template_path, method='ConstantPressure', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='MeanPressure', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='MeanFlow', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='Auto', dt=dt, totalTime=totalTime)
    run_simulation(network_template_path, method='AutoLinearSystem', dt=dt, totalTime=totalTime)
    plot_results(network_template_path, method='ConstantPressure', fmtstr='-', dt=dt)
    plot_results(network_template_path, method='MeanPressure', fmtstr='--', dt=dt, alpha=0.45)
    plot_results(network_template_path, method='MeanFlow', fmtstr='.-', dt=dt, alpha=0.35)
    #plot_results(network_template_path, method='Auto', fmtstr=':o', dt=dt, alpha=0.25)
    #plot_results(network_template_path, method='AutoLinearSystem', dt=dt, fmtstr=':^',  alpha=0.25)

    # ## OLD
    # dt=-1
    # run_simulation(network_template_path, method='ConstantPressure')
    # # run_simulation(network_template_path, method='MeanPressure')
    # # run_simulation(network_template_path, method='MeanFlow')
    # run_simulation(network_template_path, method='Auto')
    # plot_results(network_template_path, method='ConstantPressure', fmtstr='-')
    # plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    # plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    # plot_results(network_template_path, method='Auto', fmtstr=':o', alpha=0.25)
    # plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^',  alpha=0.25)
    # dt=-2
    # # run_simulation(network_template_path, method='ConstantPressure', dt=dt)
    # # run_simulation(network_template_path, method='MeanPressure', dt=dt)
    # # run_simulation(network_template_path, method='MeanFlow', dt=dt)
    # # run_simulation(network_template_path, method='Auto', dt=dt)
    # # run_simulation(network_template_path, method='AutoLinearSystem')
    # plot_results(network_template_path, method='ConstantPressure', fmtstr='-', dt=dt)
    # plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    # plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    # plot_results(network_template_path, method='Auto', fmtstr=':o', alpha=0.25, dt=dt)
    # plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^',  alpha=0.25)
    # plt.show()

    network_template_path = "../starfish/TemplateNetworks/bigDaddy_Reymond2009_template/bigDaddy_Reymond2009_template.xml"
    run_simulation(network_template_path, method='ConstantPressure')
    run_simulation(network_template_path, method='MeanPressure')
    run_simulation(network_template_path, method='MeanFlow')
    run_simulation(network_template_path, method='Auto')
    run_simulation(network_template_path, method='AutoLinearSystem')
    plot_results(network_template_path, method='ConstantPressure', fmtstr='-')
    plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    plot_results(network_template_path, method='AutoPrevCommit', fmtstr=':o', alpha=0.25)
    plot_results(network_template_path, method='Auto', fmtstr=':o', alpha=0.25)
    plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^',  alpha=0.25)
    plt.show()

    # network_template_path = "../starfish/TemplateNetworks/Full96Model_template/Full96Model_template.xml"
    # test_initialisation(network_template_path, method='ConstantPressure')
    # test_initialisation(network_template_path, method='MeanPressure')
    # test_initialisation(network_template_path, method='MeanFlow')
    #test_initialisation(network_template_path, method='Auto')
    #test_initialisation(network_template_path, method='AutoLinearSystem')
    #plot_results(network_template_path, method='ConstantPressure', fmtstr='-')
    # plot_results(network_template_path, method='MeanPressure', fmtstr='--', alpha=0.45)
    # plot_results(network_template_path, method='MeanFlow', fmtstr='.-', alpha=0.35)
    #plot_results(network_template_path, method='Auto', fmtstr=':o', alpha=0.25)
    #plot_results(network_template_path, method='AutoLinearSystem', fmtstr=':^', alpha=0.25)
    #plt.show()