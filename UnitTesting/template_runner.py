import sys
import os
import logging
import matplotlib.pyplot as plt
import h5py
logger = logging.getLogger(__name__)
cur = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS

TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6
default_method='ConstantPressure'
default_dt=-1
default_totalTime=0.1

#@profile
def run_simulation(network_template_path, method=default_method, dt=default_dt, totalTime=default_totalTime):
        networkXmlFileLoad = os.path.join(cur, network_template_path)

        networkName = os.path.basename(networkXmlFileLoad)
        networkName = os.path.splitext(networkName)[0]
        case_str = method + "_dt_" + str(dt)
        networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".xml")
        pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")
        dataNumber = 'tst'
        vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                     dataNumber,
                                                     networkXmlFile = networkXmlFileLoad,
                                                     pathSolutionDataFilename = pathSolutionDataFilename,
                                                     )
        vascularNetworkNew.dt = dt
        vascularNetworkNew.initialsationMethod = method
        vascularNetworkNew.quiet = False
        vascularNetworkNew.totalTime =totalTime

        flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=False)
        flowSolver.solve()
        vascularNetworkNew.saveSolutionData()
        mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)

def plot_results(template_path,
                 method=default_method,
                 dt=default_dt,
                 vessels_to_compare=[0, 1,2, 5, 43, 16, 70],
                 nStepsCompare=None,
                 fmtstr='-',
                 alpha=0.75
                 ):
    networkName = os.path.basename(template_path)
    networkName = os.path.splitext(networkName)[0]
    case_str = method + "_dt_" + str(dt)
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")
    print(pathSolutionDataFilename)
    solutionNew = h5py.File(pathSolutionDataFilename, "r")
    netNew = solutionNew['VascularNetwork']
    tNew = netNew['simulationTime'][:]
    print('t0', tNew[0])
    vesselsNew = solutionNew['vessels']
    for subGroupName, subGroup in vesselsNew.items():
        vesselId = int(subGroupName.split(' - ')[-1])
        if vesselId == 0:
            Qnew = subGroup['Qsol']
            plt.figure(num='flow')  # number=subGroupName)q
            plt.plot(tNew, Qnew[:, 0]*m3_to_ml, label=method, alpha=alpha)
            print(method, 'flow(t_0)', Qnew[0, 0] * m3_to_ml)
            print(method, 'flow(t_final)', Qnew[-1, 0]*m3_to_ml)
            plt.legend()

            plt.figure(num='pressure')
            Pnew = subGroup['Psol']
            plt.plot(tNew, Pnew[:, 0], label='Inlet' + method, alpha=alpha)
            plt.legend()

        if nStepsCompare is None:
            nStepsCompare = len(tNew)
        if vesselId in vessels_to_compare:
            Pnew = subGroup['Psol']
            Qnew = subGroup['Qsol']
            plt.figure(num=subGroupName+'P')
            mid_point_idx = Pnew.shape[1]//2
            plt.plot(tNew[0:nStepsCompare], Pnew[0:nStepsCompare, mid_point_idx],  fmtstr, label=method, alpha=alpha)
            plt.legend()
            plt.figure(num=subGroupName+'Q')
            plt.plot(tNew[0:nStepsCompare], Qnew[0:nStepsCompare, mid_point_idx], fmtstr, label=method)

            if vesselId in [1,2]:
                plt.figure(num='pressure')
                plt.plot(tNew, Pnew[:, -1], label='Outlet' +str(vesselId) + method, alpha=alpha)
                plt.legend()
