import os, sys
cur = os.path.dirname(os.path.realpath(__file__))
import logging
import matplotlib.pyplot as plt
import h5py
import numpy as np
logger = logging.getLogger(__name__)
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS

TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6
default_method='ConstantPressure'
default_dt=-1
default_totalTime=0.1

# conversion factors from given unit to Pa-m-kg
pascal = 1
mmHg = 133.32
m2 = 1
cm2 = 1e-4
mm2 = 1e-6
m3_per_sec = 1
ml_per_sec = 1e-6

def compare_with_reference(newNetwork, refNetwork):
    # Set which values should be tested, and the threshold for mean square error
    testDict = {
                "Pressure": {'threshold': 10.0, 'units':mmHg},
                "Area": {'threshold':1.0, 'units':cm2},
                "Flow": {'threshold':5.0, 'units':ml_per_sec}
                }

    # load reference data and link it
    newName = newNetwork.pop('networkName')
    vascularNetworkNew = mXML.loadNetworkFromXML(newName, **newNetwork)
    vascularNetworkNew.linkSolutionData()

    refName = refNetwork.pop('networkName')
    vascularNetworkRef = mXML.loadNetworkFromXML(refName, **refNetwork)
    vascularNetworkRef.linkSolutionData()

    RMSEDict = {}
    print("\nRoot Mean Square Error for each part of the network is:")
    for vesselId, vessel in vascularNetworkNew.vessels.items():
        RMSEDict[vesselId] = {}

        xvals = [vessel.z[0], vessel.z[-1]]
        dataDictNew = vascularNetworkNew.getSolutionData(vesselId,
                                                         testDict.keys(),
                                                         vascularNetworkNew.simulationTime, xvals)
        dataDictRef = vascularNetworkRef.getSolutionData(vesselId,
                                                         testDict.keys(),
                                                         vascularNetworkNew.simulationTime, xvals)

        for key, vals in testDict.items():
            testVals = np.array(dataDictNew[key])
            refVals = np.array(dataDictRef[key])
            error = testVals - refVals
            RMSEDict[vesselId][key] = np.sqrt(np.mean(error**2))/vals['units']
        print(f"{vesselId}: {RMSEDict[vesselId]}")



    TooHighError = False
    for vesselId in RMSEDict:
        for key in RMSEDict[vesselId]:
            if testDict[key]['threshold'] < RMSEDict[vesselId][key]:
                TooHighError = True
                print(
                    "Error was found to be too high for Vessel {}, key {}, with value {} being above threshold of {}".format(
                        vesselId, key, RMSEDict[vesselId][key], testDict[key]))

    assert (not TooHighError)
    if not TooHighError:
        print("\nAll values below error threshold")
        print("Test Successful!")


#@profile
def run_simulation(network_template_path, method=default_method, dt=default_dt, totalTime=default_totalTime, initial_values_path=None):
        networkXmlFileLoad = os.path.join(cur, network_template_path)

        networkName = os.path.basename(networkXmlFileLoad)
        networkName = os.path.splitext(networkName)[0]
        case_str = method + "_dt_" + str(dt)
        networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".xml")
        pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")
        dataNumber = 'tst'
        vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                     dataNumber,
                                                     networkXmlFile = networkXmlFileLoad,
                                                     pathSolutionDataFilename = pathSolutionDataFilename,
                                                     )
        vascularNetworkNew.dt = dt
        vascularNetworkNew.initialsationMethod = method
        if initial_values_path:
            vascularNetworkNew.initialValuesPath = initial_values_path
        vascularNetworkNew.quiet = False
        vascularNetworkNew.totalTime =totalTime

        flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=False)
        flowSolver.solve()
        vascularNetworkNew.saveSolutionData()
        mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)
        return dict(networkName=networkName,
                    dataNumber=dataNumber,
                    networkXmlFile=networkXmlFileSave,
                    pathSolutionDataFilename=pathSolutionDataFilename)


def plot_results(template_path,
                 method=default_method,
                 dt=default_dt,
                 vessels_to_compare=[0, 1,2, 5, 43, 16, 70],
                 nStepsCompare=None,
                 fmtstr='-',
                 alpha=0.75
                 ):
    networkName = os.path.basename(template_path)
    networkName = os.path.splitext(networkName)[0]
    case_str = method + "_dt_" + str(dt)
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")
    print(pathSolutionDataFilename)
    solutionNew = h5py.File(pathSolutionDataFilename, "r")
    netNew = solutionNew['VascularNetwork']
    tNew = netNew['simulationTime'][:]
    print('t0', tNew[0])
    vesselsNew = solutionNew['vessels']
    for subGroupName, subGroup in vesselsNew.items():
        vesselId = int(subGroupName.split(' - ')[-1])
        if vesselId == 0:
            Qnew = subGroup['Qsol']
            plt.figure(num='flow')  # number=subGroupName)q
            plt.plot(tNew, Qnew[:, 0]*m3_to_ml, label=method, alpha=alpha)
            print(method, 'flow(t_0)', Qnew[0, 0] * m3_to_ml)
            print(method, 'flow(t_final)', Qnew[-1, 0]*m3_to_ml)
            plt.legend()

            plt.figure(num='pressure')
            Pnew = subGroup['Psol']
            plt.plot(tNew, Pnew[:, 0], label='Inlet' + method, alpha=alpha)
            plt.legend()

        if nStepsCompare is None:
            nStepsCompare = len(tNew)
        if vesselId in vessels_to_compare:
            Pnew = subGroup['Psol']
            Qnew = subGroup['Qsol']
            plt.figure(num=subGroupName+'P')
            mid_point_idx = Pnew.shape[1]//2
            plt.plot(tNew[0:nStepsCompare], Pnew[0:nStepsCompare, mid_point_idx],  fmtstr, label=method, alpha=alpha)
            plt.legend()
            plt.figure(num=subGroupName+'Q')
            plt.plot(tNew[0:nStepsCompare], Qnew[0:nStepsCompare, mid_point_idx], fmtstr, label=method)

            if vesselId in [1,2]:
                plt.figure(num='pressure')
                plt.plot(tNew, Pnew[:, -1], label='Outlet' +str(vesselId) + method, alpha=alpha)
                plt.legend()
