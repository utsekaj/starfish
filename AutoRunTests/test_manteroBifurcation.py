import os, sys
cur = os.path.dirname(os.path.realpath(__file__))
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS
from utilities import compare_with_reference

TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
REFERENCE_OUTPUT_PATH = os.path.join(cur, 'reference_tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6

def test_fromFile():
    # load and run a new simulation of reference
    networkName = "manteroBifurcation"
    dataNumber = "999"
    case = dataNumberSol = "012"
    networkXmlFileLoad = os.path.join(cur, networkName, networkName+'.xml')
    networkXmlSolutionLoad = os.path.join('reference_tests_output', networkName, networkName + '_SolutionData_012.xml')
    solutionDataLoad = os.path.join(REFERENCE_OUTPUT_PATH, networkName, networkName + '_SolutionData_012.hdf5')

    # Temporary files for saving data
    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.hdf5')

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile = networkXmlFileLoad,
                                                 pathSolutionDataFilename = pathSolutionDataFilename)
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)

    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=solutionDataLoad)
    compare_with_reference(newNetwork, refNetwork)

if __name__ == "__main__":
    test_fromFile()
