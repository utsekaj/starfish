import os, sys
cur = os.path.dirname(os.path.realpath(__file__))
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS
from utilities import compare_with_reference


TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6

def test_singleVessel():
    # load and run a new simulation of reference
    networkName = "singleVessel"
    dataNumber = "999"
    case = dataNumberSol = "012"
    networkXmlFileLoad = os.path.join(cur, networkName, networkName+'.xml')
    networkXmlSolutionLoad = os.path.join(cur, networkName, networkName + '_SolutionData_012.xml')
    solutionDataLoad = os.path.join(cur, networkName, networkName + '_SolutionData_012.hdf5')

    # Temporary files for saving data
    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.hdf5')

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile = networkXmlFileLoad,
                                                 pathSolutionDataFilename = pathSolutionDataFilename)
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)

    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=solutionDataLoad)
    compare_with_reference(newNetwork, refNetwork)

def test_singleVessel_BCFile():
    # load and run a new simulation of reference
    networkName = "singleVessel"
    dataNumber = "999"
    case = dataNumberSol = "bcd"
    networkXmlFileLoad = os.path.join(cur, networkName, networkName+'FromFile.xml')
    networkXmlFileLoad = os.path.join(cur, networkName, networkName + 'FromFolder.xml')
    networkXmlSolutionLoad = os.path.join(cur, networkName, networkName + '_SolutionData_012.xml')
    solutionDataLoad = os.path.join(cur, networkName, networkName + '_SolutionData_012.hdf5')

    # Temporary files for saving data
    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + f'_SolutionData_{case}.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + f'_SolutionData_{case}.hdf5')

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile=networkXmlFileLoad,
                                                 pathSolutionDataFilename = pathSolutionDataFilename)
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)
    assert os.path.isdir(os.path.join(os.path.dirname(networkXmlFileSave), 'InputData')), 'Missing input data from output'

def test_singleVesselVaryingElastance():

    # load and run a new simulation of reference
    networkName = "singleVesselVE"
    dataNumber = "999"
    dataNumberSol = "012"
    networkXmlFileLoad = cur + "/singleVesselVE/singleVesselVE.xml"
    networkXmlSolutionLoad = cur + "/singleVesselVE/singleVesselVE_SolutionData_012.xml"
    solutionDataLoad = cur + "/singleVesselVE/singleVesselVE_SolutionData_012.hdf5"
    # Temporary files for saving data
    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_999.hdf5')


    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile=networkXmlFileLoad,
                                                 pathSolutionDataFilename=pathSolutionDataFilename)
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)
    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=solutionDataLoad)
    compare_with_reference(newNetwork, refNetwork)

def test_saveSkipping():
    # load and run a new simulation of reference
    networkName = "singleVessel"
    dataNumber = "skp"
    dataNumberSol = "012"
    networkXmlFileLoad = cur + "/singleVessel/singleVessel.xml"
    networkXmlSolutionLoad = cur + "/singleVessel/singleVessel_SolutionData_012.xml"
    solutionDataLoad = cur + "/singleVessel/singleVessel_SolutionData_012.hdf5"
    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_' +dataNumber +'.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_' + dataNumber + '.hdf5')

    vascularNetworkPrev = mXML.loadNetworkFromXML(networkName,
                                                 dataNumberSol,
                                                 networkXmlFile=networkXmlSolutionLoad,
                                                 pathSolutionDataFilename=solutionDataLoad)

    # modified minSaveDT #TODO do we need this?
    previousDT = vascularNetworkPrev.saveDt
    previousCFL = vascularNetworkPrev.CFL
    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile = networkXmlFileLoad,
                                                 pathSolutionDataFilename = pathSolutionDataFilename)
    vascularNetworkNew.quiet = True
    vascularNetworkNew.minDt = previousDT
    vascularNetworkNew.CFL = previousCFL/2.
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)

    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=solutionDataLoad)
    compare_with_reference(newNetwork, refNetwork)

def test_memoryChunking():
    # load and run a new simulation of reference
    networkName = "singleVessel"
    dataNumber = "cnk"
    dataNumberSol = "012"
    networkXmlFileLoad = cur + "/singleVessel/singleVessel.xml"
    networkXmlSolutionLoad = cur + "/singleVessel/singleVessel_SolutionData_012.xml"
    solutionDataLoad = cur + "/singleVessel/singleVessel_SolutionData_012.hdf5"
    # modified maxMemory to force chunking

    output_dir = os.path.join(TEST_OUTPUT_PATH, networkName)
    os.makedirs(output_dir, exist_ok=True)
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_' +dataNumber +'.xml')
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName + '_SolutionData_' + dataNumber+'.hdf5')

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile = networkXmlFileLoad,
                                                 pathSolutionDataFilename = pathSolutionDataFilename)
    vascularNetworkNew.maxMemory = 0.00001
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)


    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=solutionDataLoad)
    compare_with_reference(newNetwork, refNetwork)

if __name__ == "__main__":
    # test_singleVesselVaryingElastance()
    test_singleVessel_BCFile()
    # test_saveSkipping()
    # test_memoryChunking()
