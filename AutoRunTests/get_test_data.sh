rm -r test_working_directory
mkdir test_working_directory
HOST=https://folk.ntnu.no/jacobts/
cd Full96Model
wget -q -N $HOST'/stf_test_data/Full96Model/Full96Model.hdf5' 2>/tmp/err.log || cat /tmp/err.log; rm /tmp/err.log
cd ../AoBif
wget -q -N $HOST'/stf_test_data/AoBif/AoBif_sol.hdf5' 2>/tmp/err.log || cat /tmp/err.log; rm /tmp/err.log
wget -q -N $HOST'/stf_test_data/AoBif/AoBif_tmpSol.hdf5' 2>/tmp/err.log || cat /tmp/err.log; rm /tmp/err.log
cd ..
wget -q -N -r -np -nH --cut-dirs=3 -R "index.html*" $HOST'/stf_test_data/reference_tests_output/' 2>/tmp/err.log || cat /tmp/err.log; rm /tmp/err.log
wget -q -N -r -np -nH --cut-dirs=3 -R "index.html*" $HOST'/stf_test_data/SuppMaterial_1DBenchmark_Boileau_etal/' 2>/tmp/err.log || cat /tmp/err.log; rm /tmp/err.log
