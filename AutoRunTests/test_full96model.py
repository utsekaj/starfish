import os
import logging
import subprocess

formatter = logging.Formatter('%(name)s - %(lineno)d -  %(levelname)s - %(message)s')
logger = logging.getLogger('starfish')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("test_full96model.log", mode="w") #TODO better formatting
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
stderr = logging.StreamHandler()
stderr.setLevel(logging.DEBUG)
stderr.setFormatter(formatter)
logger.addHandler(stderr)

from utilities import run_simulation, compare_with_reference

def test_init_from_file():
    network_template_path = 'Full96Model/Full96Model.xml'
    initial_values_path = 'InitialValues/' # TODO this must be an absolute path or relative to the directory containing the XML
    newNetwork = run_simulation(network_template_path, method='FromSolution', initial_values_path=initial_values_path, dt=-2, totalTime=0.1)

    # newNetwork = dict(networkName=os.path.splitext(os.path.basename(network_template_path))[0],
    #                   dataNumber='new',
    #                   networkXmlFile='Full96Model.FromSolution_dt_-2.xml',
    #                   pathSolutionDataFilename='Full96Model.FromSolution_dt_-2.hdf5')

    refNetwork = dict(networkName=os.path.splitext(os.path.basename(network_template_path))[0],
                      dataNumber='ref',
                      networkXmlFile=os.path.join(os.path.dirname(network_template_path), 'Full96Model.FromSolution_dt_-2.xml'),
                      pathSolutionDataFilename=os.path.join(os.path.dirname(network_template_path), 'Full96Model.FromSolution_dt_-2.hdf5'))
    compare_with_reference(newNetwork, refNetwork)


if __name__ == "__main__":
    test_init_from_file()
