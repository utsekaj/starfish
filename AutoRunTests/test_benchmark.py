"""
defing a command 
https://stackoverflow.com/a/23659385
pytest --tb=native
pytest --tb=long
script -q -c "vagrant up" test_results.txt
pytest -v --tb=long -n auto --durations=0 --html=report.html --self-contained-html test_benchmark.py
pytest -v --tb=long  -n auto --durations=0 --html=report_adan.html --self-contained-html test_benchmark.py
pytest -v --tb=long  -n auto --durations=0 --html=report_adan.html --self-contained-html --cov=starfish --cov-report html test_benchmark.py
pycallgraph -f svg -o pycallgraph.svg mine.py
"""
import os
import sys
import subprocess
import pytest
import logging
formatter = logging.Formatter('%(name)s - %(lineno)d -  %(levelname)s - %(message)s')
logger = logging.getLogger('starfish')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("test_benchmark.log", mode="w") #TODO better formatting
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
stderr = logging.StreamHandler()
stderr.setLevel(logging.DEBUG)
stderr.setFormatter(formatter)
logger.addHandler(stderr)
import numpy as np

import starfish
import starfish.NetworkLib.classBoundaryConditions
from starfish.Simulator import main
import starfish.UtilityLib.moduleXML as mXML
DATAPATH = 'SuppMaterial_1DBenchmark_Boileau_etal'
pa_to_kpa = 1e-3
m3_to_ml = 1e6

from utilities import run_simulation, default_totalTime, compare_with_reference

def test_singlePulse(networkName = 'singleVesselPulse_template'):
    PTOL = 10
    RTOL = 0.025
    singlePulse = 'Benchmark1_SinglePulseModel'
    data = np.genfromtxt(os.path.join(DATAPATH, singlePulse, 'NumData', 'McC_viscous.txt'))

    dataNumber = 'vis'
    simulationDescription = 'viscous'
    # networkName = 'singleVesselPulse'
    resimulate = False
    main(networkName, dataNumber, simulationDescription, resimulate)

    refNetwork=dict(networkName=networkName.split('_template')[0],  # TODO better handle the "template" case
                      dataNumber=dataNumber,
                      networkXmlFile=None,
                      pathSolutionDataFilename=None)
    refName = refNetwork.pop('networkName')
    vascularNetworkRef = mXML.loadNetworkFromXML(refName, **refNetwork)
    vascularNetworkRef.linkSolutionData()

    RMSEDict = {}
    testDict = {"Pressure": {'threshold': 10.0, 'units': 'mmHg'}}
    tVals = [0.1, 0.3, 0.5, 0.7, 0.9, 1.1, 1.3, 1.5]
    xVals = data[:, 0]
    data = data[:,1:]
    vesselId = 0
    t_idx = np.array([0, 1, 2])
    dataDictNew = vascularNetworkRef.getSolutionData(vesselId,
                                                     ['Pressure'],
                                                     tVals,
                                                     xVals)
                                                     #extrap=True)

    Psol = dataDictNew['Pressure'].T * pa_to_kpa
    maxP = np.max(Psol)
    Perr = Psol - data
    Prerr = Perr / maxP

    assert np.max(np.abs(Perr)) < PTOL, 'Maximum difference in pressure too large'
    assert np.mean(np.abs(Prerr)) < RTOL, 'Average relative difference in pressure too large'
    Psol = Psol / maxP
    data /= np.max(data[:, t_idx])
    for idx, t in enumerate(tVals):
        peak_x = np.argmax(Psol[:, idx])
        data_x = np.argmax(data[:, idx])
        assert np.abs(data_x - peak_x) < 5, f'Peak location at t={t} differs too much'
        assert np.abs(np.max(Psol[:, idx]) - np.max(data[:, idx])) < PTOL, f'Peak pressure at t={t} differs too much'

    vascularNetworkRef.solutionDataFile.close()
    del vascularNetworkRef
    # TODO add inviscid case
    # data = np.genfromtxt(os.path.join(DATAPATH, DATAPATH, singlePulse, 'NumData', 'McC_inviscid.txt'))


def test_singleCarotid(interactive=False):
    PTOL = 1 # kPa
    QTOL = 1 # ml/Sec
    PRTOL = 0.01
    QRTOL = 0.01
    datafile= 'Benchmark2_CommonCarotidArtery/NumData/McC.txt'
    #datafile = 'Benchmark2_CommonCarotidArtery/NumData/3D.txt' #TODO add 3D comparison?

    data = np.genfromtxt(os.path.join(DATAPATH, datafile))

    dataNumber = 'tst'
    simulationDescription = 'tst'
    networkName = 'singleCarotid_template'
    resimulate = False
    main(networkName, dataNumber, simulationDescription, resimulate)
    refNetwork = dict(networkName=networkName.split('_template')[0], # TODO better handle the "template" case
                      dataNumber=dataNumber,
                      networkXmlFile=None,
                      pathSolutionDataFilename=None)
    refName = refNetwork.pop('networkName')
    vascularNetworkRef = mXML.loadNetworkFromXML(refName, **refNetwork)
    vascularNetworkRef.linkSolutionData()

    RMSEDict = {}
    testDict = {"Pressure": {'threshold': 10.0, 'units': 'mmHg'},
                "Flow":None}
    tVals = data[:,0]
    period = 1.1
    comparison_mask = tVals > 8*period
    t_offset = 0 #
    tVals1D = data[:,0] # Defined for eventual comparison with 3D
    xVals = [0.126/2] # Midpoint
    vesselId = 0
    dataDictNew = vascularNetworkRef.getSolutionData(vesselId,
                                                     testDict.keys(),
                                                     tVals1D+t_offset,
                                                     xVals,
                                                     extrap=True)

    Psol = dataDictNew['Pressure'].flatten()
    Perr = Psol[comparison_mask] * pa_to_kpa - data[comparison_mask, 1]
    Prerr = Perr / data[comparison_mask, 1]
    assert np.max(np.abs(Perr)) < PTOL, 'Maximum difference in pressure too large'
    assert np.mean(np.abs(Prerr)) < PRTOL, 'Average relative difference in pressure too large'

    print(
        f'Pressure Error [kPa]: Max={np.max(np.abs(Perr))} MAE={np.mean(np.abs(Perr))} RMSE={np.sqrt(np.mean(Perr ** 2))}')
    print('Pressure Error',
          np.linalg.norm((Psol[comparison_mask] * pa_to_kpa - data[comparison_mask, 1]) / len(comparison_mask)))

    Qsol = dataDictNew['Flow'].flatten()
    Qerr = Qsol[comparison_mask] * m3_to_ml - data[comparison_mask, 2]
    Qrerr = Qerr / data[comparison_mask, 2]
    assert np.max(np.abs(Qerr)) < QTOL, 'Maximum difference in pressure too large'
    assert np.mean(np.abs(Qrerr)) < QRTOL, 'Average relative difference in pressure too large'
    if interactive:
        import matplotlib.pyplot as plt
        plt.figure()
        plt.plot(tVals, data[:, 1], color='C0', alpha=0.25)
        plt.plot(tVals1D, Psol[:,0]*pa_to_kpa, color='C1', alpha=0.5)
        plt.figure()
        plt.plot(tVals, data[:, 2], color='C0', alpha=0.25)
        plt.plot(tVals1D, Qsol[:,0]*m3_to_ml, color='C1', alpha=0.5)
        plt.show()

@pytest.mark.skipif('RUN_LONG_TESTS' not in os.environ, reason='Default to fast testing')
def test_adan56(networkName='adan56_benchmark2015_template'):
    vessel_names = {'AorticArchI': 'aortic_arch_I',
                    'ThoAortaIII': 'thoracic_aorta_III',
                    'AbdomAortaV': 'abdominal_aorta_V',
                    'RightCCA': 'common_carotid_R',
                    'RightRenal': 'renal_R',
                    'RightCommonIliac': 'common_iliac_R',
                    'RightICA': 'internal_carotid_R',
                    'RightRadial': 'radial_R',
                    'RightInternalIliac': 'internal_iliac_R',
                    'RightPOstInteross': 'posterior_interosseous_R',
                    'RightFemoralII': 'femoral_II_R',
                    'RightAntTibial': 'anterior_tibial_R'}
    dataNumber = '100'
    simulationDescription = 'slightly smaller time step'

    resimulate = False
    main(networkName, dataNumber, simulationDescription, resimulate)
    refNetwork = dict(networkName=networkName.split('_template')[0],  # TODO better handle the "template" case
                      dataNumber=dataNumber,
                      networkXmlFile=None,
                      pathSolutionDataFilename=None)
    refName = refNetwork.pop('networkName')
    vascularNetworkRef = mXML.loadNetworkFromXML(refName, **refNetwork)
    vascularNetworkRef.linkSolutionData()
    period = 1.0
    datafile = 'Benchmark6_ADAN56/NumData/McC_P.txt'
    data = np.genfromtxt(os.path.join(DATAPATH, datafile))
    RMSEDict = {}
    testDict = {"Pressure": {'threshold': 10.0, 'units': 'mmHg'}}
    tVals = data[:, 0]

    comparison_mask = (tVals > 8*period) & (tVals<9*period)
    t_offset = 0.0 # 0.0421 # TODO get from simluation directly
    Perr_dict = dict()
    for idx, (data_name, stf_name) in enumerate(vessel_names.items()):
        for vesselId, vessel in vascularNetworkRef.vessels.items():
            #print(vesselId, vessel.name)
            if vessel.name == stf_name:
                xval = vessel.length/2
                dataDictNew = vascularNetworkRef.getSolutionData(vesselId,
                                                                 testDict.keys(),
                                                                 tVals+t_offset,
                                                                 [xval],
                                                                 extrap=True)


                Psol = dataDictNew['Pressure']
                Perr = Psol[comparison_mask,0] * pa_to_kpa - data[comparison_mask, idx+1]
                Prerr = Perr/np.mean(data[comparison_mask, idx+1])
                Perr_dict[data_name] = dict(Max=np.max(np.abs(Perr)),
                                            Bias=np.mean(Perr),
                                            MAE=np.mean(np.abs(Perr)),
                                            RMSE=np.sqrt(np.mean(Perr ** 2)),
                                            MRE=np.mean(np.abs(Prerr)))
                ADAN_P_TOL=1
                ADAN_P_RTOL=0.025
                assert np.max(np.abs(Perr)) < ADAN_P_TOL, f'Max pressure difference too high in {data_name}'
                assert np.mean(np.abs(Prerr)) < ADAN_P_RTOL, f'Average relative pressure difference too high in {data_name}'

    datafile = 'Benchmark6_ADAN56/NumData/McC_Q.txt'
    data = np.genfromtxt(os.path.join(DATAPATH, datafile))
    RMSEDict = {}
    testDict = {"Flow": {'threshold': 10.0, 'units': 'mmHg'}}
    tVals = data[:, 0]
    Qerr_dict = dict()
    for idx, (data_name, stf_name) in enumerate(vessel_names.items()):
        for vesselId, vessel in vascularNetworkRef.vessels.items():
            # print(vesselId, vessel.name)
            if vessel.name == stf_name:
                xval = vessel.length / 2
                dataDictNew = vascularNetworkRef.getSolutionData(vesselId,
                                                                 testDict.keys(),
                                                                 tVals + t_offset,
                                                                 [xval],
                                                                 extrap=True)

                Qsol = dataDictNew['Flow']
                Qerr = Qsol[comparison_mask,0] * m3_to_ml - data[comparison_mask, idx+1]
                Qrerr = Qerr / np.mean(data[comparison_mask, idx+1])
                Qerr_dict[data_name] = dict(Max=np.max(np.abs(Qerr)),
                                            MAE=np.mean(np.abs(Qerr)),
                                            RMSE=np.sqrt(np.mean(Qerr ** 2)),
                                            MRE=np.mean(np.abs(Qrerr))) #, MRE=)
                ADAN_Q_TOL=20
                ADAN_Q_RTOL = 0.05
                assert np.max(np.abs(Perr)) < ADAN_P_TOL, f'Max flow difference too high in {data_name}'
                assert np.mean(np.abs(Prerr)) < ADAN_P_RTOL, f'Average relative flow difference too high in {data_name}'