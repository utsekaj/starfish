import sys, os
cur = os.path.dirname(os.path.realpath(__file__))
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS
from utilities import compare_with_reference

TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)

def test_singleVessel():
    # Set which values should be tested, and the threshold for mean square error
    testDict = {"Pressure": 10.0,
                "Area": 10.0,
                "Flow": 10.0}
    # load and run a new simulation of reference
    networkName = "singleBifurcation"
    dataNumber = "999"
    dataNumberSol = "001"
    networkXmlFileLoad = os.path.join(cur, networkName, f"{networkName}.xml")
    networkXmlSolutionLoad = os.path.join(cur, networkName, f"{networkName}_SolutionData_{dataNumberSol}.xml")

    # Temporary files for saving data
    networkXmlFileSave = os.path.join(TEST_OUTPUT_PATH, networkName,  networkName +".xml")
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName, networkName +".hdf5")
    os.makedirs(os.path.dirname(pathSolutionDataFilename), exist_ok=True)

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 dataNumber,
                                                 networkXmlFile=networkXmlFileLoad,
                                                 pathSolutionDataFilename=pathSolutionDataFilename)
    print(networkXmlFileLoad)
    print(vascularNetworkNew.totalTime)
    vascularNetworkNew.quiet = True
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    vascularNetworkNew.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetworkNew, dataNumber, networkXmlFileSave)

    newNetwork = dict(networkName=networkName,
                      dataNumber=dataNumber,
                      networkXmlFile=networkXmlFileSave,
                      pathSolutionDataFilename=pathSolutionDataFilename)

    refNetwork = dict(networkName=networkName,
                      dataNumber=dataNumberSol,
                      networkXmlFile=networkXmlSolutionLoad,
                      pathSolutionDataFilename=pathSolutionDataFilename)
    compare_with_reference(newNetwork, refNetwork)

if __name__ == "__main__":
    test_singleVessel()
