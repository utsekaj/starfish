import numpy as np
import starfish.VascularPolynomialChaosLib.chaospy_wrapper as cpw
cp = cpw.cp
def test_ishigami():
    atol=1e-2
    rtol=1e-2
    # http://www.sfu.ca/~ssurjano/ishigami.html
    # Sobol and Levitan 1999 use a=7 and b=0.05 which works well
    # http://www.andreasaltelli.eu/file/repository/Sobol_Levitan_1999.pdf
    # For a=7 and b=0.1(Marrel et al 2009 Gaussian Processes) seems to be much
    # more challenging for accurate total order indices
    a = 7.
    b = 0.05  # 5

    # 1
    # b = 0.10
    def wrapper(z):
        return np.sin(z[0]) + a * np.sin(z[1]) ** 2 + b * z[2] ** 4 * np.sin(z[0])

    D = a ** 2. / 8 + b * np.pi ** 4. / 5 + b ** 2 * np.pi ** 8. / 18 + 1. / 2
    D1 = b * np.pi ** 4. / 5 + b ** 2 * np.pi ** 8. / 50. + 1. / 2
    D2 = a ** 2 / 8.
    D3 = 0

    D12 = 0
    D13 = b ** 2. * np.pi ** 8 / 18 - b ** 2 * np.pi ** 8. / 50.
    D23 = 0
    D123 = 0

    input_names = ["z1", "z2", "z3"]
    s_true = [D1 / D, D2 / D, D3 / D]
    s_t_true = [(D1 + D13) / D, D2 / D, D13 / D]

    jpdf = cp.Iid(cp.Uniform(-np.pi, np.pi), 3)

    polynomial_order = 6  # 12 #2 #4 # Order 6 needed to pass the tolerances
    basis = cpw.generate_basis(polynomial_order, jpdf)
    # 2. generate samples with random sampling
    Ns_pc = 4 * len(basis['poly'])
    samples_pc = jpdf.sample(size=Ns_pc, rule='S')
    # 3. evaluate the model, to do so transpose samples and hash input data
    # transposed_samples = samples_pc.transpose()
    model_evaluations = wrapper(samples_pc)
    model_evaluations = model_evaluations
    # 4. calculate generalized polynomial chaos expression
    gpce_regression = cpw.fit_regression(basis, samples_pc, model_evaluations)
    stats = cpw.calc_descriptives(gpce_regression)

    assert np.allclose(stats['variance'], D, atol=1, rtol=0.1), (stats['variance'], D)
    Spc = stats['sens_m']
    Stpc = stats['sens_t']
    assert np.allclose(Spc, s_true, atol=atol, rtol=rtol), (Spc, s_true)
    assert np.allclose(Stpc, s_t_true, atol=atol, rtol=rtol), (Stpc, s_t_true)

    S2_dict, _ = cpw.calc_sensitivity_indices(gpce_regression, 2)
    assert np.allclose(S2_dict[(0, 2)], D13 / D, atol=atol, rtol=rtol)


def test_high_dims(dims=10):
    """ TODO: Write test to confirm functionality for ndarray quantities of interest"""
    w = (np.arange(dims) + 1) / dims
    s_true = w ** 2 / np.sum(w ** 2)
    s_true = np.array((s_true, s_true[::-1])).T
    s_t_true = w ** 2 / np.sum(w ** 2)
    s_t_true = np.array((s_t_true, s_t_true[::-1])).T

    # w.append(1-np.sum(w))
    def wrapper(z):
        """ wrapper for samples from chaospy, i.e. first index (axis=0) iterates over variables"""
        ret_val = np.array((w @ z, w[::-1] @ z)).T
        return ret_val

    jpdf = cp.Iid(cp.Uniform(0, 1), dims)
    mean_true = np.sum(w) / 2
    variance_true = np.sum(w ** 2) / 12

    polynomial_order = 2  # Should be exact with order 1, but good to test with quadractic
    basis = cpw.generate_basis(polynomial_order, jpdf)
    # 2. generate samples with Sobol sampling
    Ns_pc = 4 * len(basis['poly'])
    samples_pc = jpdf.sample(size=Ns_pc, rule='S')
    # 3. evaluate the model, to do so transpose samples and hash input data
    model_evaluations = wrapper(samples_pc)
    print(model_evaluations.shape)
    # 4. calculate generalized polynomial chaos expression
    gpce_regression = cpw.fit_regression(basis, samples_pc, model_evaluations)
    stats = cpw.calc_descriptives(gpce_regression)

    assert np.allclose(stats['mean'], mean_true)
    assert np.allclose(stats['variance'], variance_true)
    Spc = stats['sens_m']
    Stpc = stats['sens_t']
    assert np.allclose(Spc, s_true)
    assert np.allclose(Stpc, s_t_true)


    Spc = stats['sens_m']
    Stpc = stats['sens_t']

    for idx in range(dims):
        assert np.allclose(s_true[idx], Spc[idx])
    print("S_t,i--------------------------------------------------------------------")
    for idx in range(dims):
        assert np.allclose(s_t_true[idx], Stpc[idx])


def test_array_output():
    """ TODO: Write test to confirm functionality for ndarray quantities of interest"""
    w = [0.25, 0.4]
    w.append(1 - np.sum(w))

    def wrapper(z):
        """ wrapper for samples from chaospy, i.e. first index (axis=0) iterates over variables"""
        c1 = np.sum(z, axis=0)
        c2 = np.product(z, axis=0)
        c3 = np.sqrt(w[0]) * z[0] + np.sqrt(w[1]) * z[1] + np.sqrt(w[2]) * z[2]
        return np.swapaxes([c1, c2, c3], 0, -1)

    jpdf = cp.Iid(cp.Uniform(-np.pi, np.pi), 3)
    mean_true = [0, 0, 0]
    variance_true = [3 * (2 * np.pi) ** 2 / 12, np.NaN, (2 * np.pi) ** 2 / 12]

    s_true = np.array([[1 / 3, 0, w[0]],
                       [1 / 3, 0, w[1]],
                       [1 / 3, 0, w[2]]])

    s_t_true = np.array([[1 / 3, 1, w[0]],
                         [1 / 3, 1, w[1]],
                         [1 / 3, 1, w[2]]])

    polynomial_order = 3  # If order=len(jpdf) should be exact
    basis = cpw.generate_basis(polynomial_order, jpdf)
    # 2. generate samples with random sampling
    Ns_pc = 4 * len(basis['poly'])
    samples_pc = jpdf.sample(size=Ns_pc, rule='S')
    # 3. evaluate the model, to do so transpose samples and hash input data
    # transposed_samples = samples_pc.transpose()
    model_evaluations = wrapper(samples_pc)
    # 4. calculate generalized polynomial chaos expression
    gpce_regression = cpw.fit_regression(basis, samples_pc, model_evaluations)
    stats = cpw.calc_descriptives(gpce_regression)

    assert np.allclose(stats['mean'], mean_true)
    assert np.allclose(stats['variance'][0], variance_true[0])
    assert np.allclose(stats['variance'][2], variance_true[2])
    Spc = stats['sens_m']
    Stpc = stats['sens_t']
    assert np.allclose(Spc, s_true)
    assert np.allclose(Stpc, s_t_true)

    S2_dict, _ = cpw.calc_sensitivity_indices(gpce_regression, 2)

