import os
import starfish.UtilityLib.moduleXML as mXML
import starfish.UtilityLib.moduleCSV as mCSV

def test_module_csv():
    # load and run a new simulation of reference
    networkName = "singleBifurcation"
    dataNumber = "csv"
    networkXmlFileLoad = os.path.join(networkName, f"{networkName}.xml")
    vascularNetwork = mXML.loadNetworkFromXML(networkName,
                                              dataNumber,
                                              networkXmlFile=networkXmlFileLoad)
    boundaryConditions = vascularNetwork.getVariableValue('boundaryConditions')
    mCSV.writeVesselDataToCSV(networkName, vascularNetwork.vessels)
    mCSV.writeBCToCSV(networkName, boundaryConditions)
    vesselData = mCSV.readVesselDataFromCSV(networkName)
    vascularNetwork.updateNetwork(vesselData)
    boundaryConditions = mCSV.readBCFromCSV(networkName)
    vascularNetwork.update({'boundaryConditions': boundaryConditions})
    mXML.writeNetworkToXML(vascularNetwork, dataNumber=dataNumber)
    # TODO verify a subset of properties match


if __name__ == "__main__":
    test_module_csv()
