
# Profile and benchmark single vessel with various BCs
import os
import pytest
cur = os.path.dirname(os.path.realpath(__file__))
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS


TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6


@pytest.mark.parametrize("networkName", ['singleVessel',
                                 'singleCarotid',
                                  'singleBifurcation',
                                  'huberts_pulse_2012'
                                  ]
                                  )
@pytest.mark.parametrize("dt", [-2,])
def test_singleVessel(networkName, dt):
    dataNumber = "999"
    case_str = str(dt)
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")
    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 pathSolutionDataFilename=pathSolutionDataFilename)
    vascularNetworkNew.dataNumber = dataNumber
    vascularNetworkNew.quiet = True
    vascularNetworkNew.dt = dt
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
# Profile and benchmark singleBifurcation
