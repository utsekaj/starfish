import git
import os
import shutil
import pytest
stf_repo = git.Repo('/home/jsturdy/Biomech/starfish3.10')
if stf_repo.is_dirty():
    print('warn')
    exit()


try:
   shutil.rmtree('.benchmarks')
except Exception as e:
    print(e)
# stf_repo.git.checkout('main')
# retcode = pytest.main(["--benchmark-autosave", "benchmark_skeleton.py"])

N_prev = 2
branch_name = 'simplify_BC'

for commit in reversed(range(1, N_prev)):
    stf_repo.git.checkout(f'{branch_name}~{commit}')
    try:
        retcode = pytest.main(["--benchmark-autosave",  "benchmark_skeleton.py"])
    except Exception:
        pass
    print(commit, retcode)

stf_repo.git.checkout(f'{branch_name}')
retcode = pytest.main(["--benchmark-autosave", "benchmark_skeleton.py"])