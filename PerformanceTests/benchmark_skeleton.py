
# Profile and benchmark single vessel with various BCs
import os
import pytest
cur = os.path.dirname(os.path.realpath(__file__))
import starfish.UtilityLib.moduleXML as mXML
import starfish.SolverLib.class1DflowSolver as c1dFS


TEST_OUTPUT_PATH = os.path.join(cur, 'tests_output')
os.makedirs(TEST_OUTPUT_PATH, exist_ok=True)
m3_to_ml = 1e6


def run_singleVessel(networkName, dt, runtimeEvaluation=False):
    dataNumber = "999"
    case_str = str(dt)
    case_str += str(runtimeEvaluation)
    pathSolutionDataFilename = os.path.join(TEST_OUTPUT_PATH, networkName + "." + case_str + ".hdf5")

    vascularNetworkNew = mXML.loadNetworkFromXML(networkName,
                                                 pathSolutionDataFilename=pathSolutionDataFilename)
    vascularNetworkNew.dataNumber = dataNumber
    vascularNetworkNew.quiet = True
    vascularNetworkNew.dt = dt
    for bc_list in vascularNetworkNew.boundaryConditions.values():
        for bc in bc_list:
            try:
                bc.runtimeEvaluation = runtimeEvaluation
            except AttributeError:
                pass
    flowSolver = c1dFS.FlowSolver(vascularNetworkNew, quiet=True)
    flowSolver.solve()
    del flowSolver
    del vascularNetworkNew

@pytest.mark.parametrize("networkName", ['singleVessel',
                                         # 'singleVesselDAE',
                                 'singleCarotid',
                                  'singleBifurcation',
                                  'huberts_pulse_2012'
                                  ]
                                  )
@pytest.mark.parametrize('runtimeEvaluation', [True, False])
@pytest.mark.parametrize("dt", [-2])
def test_singleVessel(benchmark, networkName, dt, runtimeEvaluation):
    benchmark.pedantic(run_singleVessel, args=(networkName, dt, runtimeEvaluation))
# Profile and benchmark singleBifurcation
