from __future__ import print_function, absolute_import
from future.utils import iteritems, iterkeys, viewkeys, viewitems, itervalues, viewvalues
from builtins import input as input3
import psutil, os
import subprocess

def memoryUsagePsutil():
    '''
    evaluate the current memory usage from psutils
    
    Returns: memory currently used in MB
    '''
    process = psutil.Process(os.getpid())
    return process.memory_info()[0] / float(2 ** 20)

def getGitHash():
    '''
    This function evaluates the actual branch and leatest git commit hash
    to create an unique version reference for xml files
    
    
    Returns: git hash (str): latest git commit hash
    '''
   
    #branchName   = subprocess.check_output(["git", "describe",'--all'])
    
    try:
        gitHash = subprocess.run(["git", "rev-parse", "HEAD"], capture_output=True, check=True, universal_newlines=True)
        gitHash = gitHash.stdout.split('\n')[0]
    except subprocess.CalledProcessError as e:
        print(e)
        gitHash = "not available"

    try:
        dirtyRepos = subprocess.run(["git", "diff", "--quiet", "--exit-code"], capture_output=True, check=True)
    except subprocess.CalledProcessError as e:
        print(e)
        print("""WARNING: moduleHelperFunctions.getGitHash(): uncommited changes detected,
         git hash does not correspond to actual code version!""")
        gitHash = ' '.join([gitHash, 'uncomitted changes'])
    return gitHash
