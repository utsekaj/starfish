#############################################################################
#
# moduleFilePathHandler
#
# provide functions to save, load, manipulate, parse etc. pickle files of simulation cases
#
# loadSolutionDataFile(networkName,dataNumbers)
# parseDirectoryForSimulationCases(networkName)
# updateSimulationDescriptions(networkName)
#
#
# created by Vinzenz Eck // vinzenz.g.eck@ntnu.no
##
from __future__ import print_function, absolute_import

import glob
from builtins import range
from future.utils import iteritems
from builtins import input as input3
import os
import sys
import shutil
from lxml import etree
import pickle
cur = os.path.dirname( os.path.realpath( __file__ ) )

import starfish
STARFISHconfig = 'STARFiSh.config'
try:
    import configparser as ConfigParser
except ImportError:
    import ConfigParser

try:
    from lxml import etree
except:
    # TODO: This produces an error!! with the pretty_print argument
    from xml.etree import ElementTree as etree


# TODO: (einar) Rename input variable exception and corresponding strings
def getFilePath(fileType, networkName, dataNumber, mode, exception = 'Error'):
    """
    Function return a requested file path, if this file exists

    Args:
        
        fileType (str):
            'randomVariableCSVFile'
            'vesselCSVFile',
            'boundaryCSVFile',
            'networkXmlFileTemplate',
            'networkXmlFile',
            'networkLogFile',
            'solutionFile',
            'configFile',
            'simulationDescriptionFile',
            'vncRescentNetworksFile',
            'vncNetworkGraphFile

        networkName (str): name of the network file

        dataNumber (int): data number of solution file or xml file

        mode (str): 'read' or 'write'


    Returns:
        string: file path

    Raises:
        Error (default): (in read mode) raise error and exit if file is not exiting
        Warning: (in read mode) just raise Warning and return with error string
    """    
    
    existingFileTypes = ['randomVariableCSVFile',
                         'vesselCSVFile',
                         'boundaryCSVFile',
                         'networkXmlFileTemplate',
                         'networkXmlFile',
                         'networkXmlFileConvergence',
                         'networkLogFile',
                         'solutionFile',
                         'solutionFileConvergence',
                         'configFile',
                         'simulationDescriptionFile',
                         'vncRescentNetworksFile',
                         'vncNetworkGraphFile']
    
    if fileType not in existingFileTypes:
        raise ValueError("ERROR: getFilePath, requested file type {}\
                          is not in existingFileTypess {}".format(fileType, existingFileTypes))
    
    if '_template' in networkName:
        if fileType == 'networkXmlFile':
            fileType = 'networkXmlFileTemplate'
    
    # file names
    filenames = {
                 'configFile'                : STARFISHconfig,
                 'vesselCSVFile'             : ''.join([networkName,'.csv']),
                 'boundaryCSVFile'           : ''.join([networkName,'BC.csv']),
                 'networkXmlFileTemplate'    : ''.join([networkName,'.xml']),
                 'networkXmlFileXXX'         : ''.join([networkName,'.xml']),
                 'networkXmlFileSim'         : ''.join([networkName,'_SolutionData_',dataNumber,'.xml']),
                 'networkXmlFileConvergence' : ''.join([networkName,'_SolutionData_',dataNumber,'.xml']),
                 'networkLogFile'            : ''.join([networkName,'_SolutionData_',dataNumber,'.tex']),
                 'solutionFileConvergence'   : ''.join([networkName,'_SolutionData_',dataNumber,'.hdf5']),
                 'solutionFile'              : ''.join([networkName,'_SolutionData_',dataNumber,'.hdf5']),
                 'simulationDescriptionFile' : ''.join(['simulationCaseDescriptions.txt']),
                 'vncRescentNetworksFile'    : '.recentNetworkNames.pickle',
                 'vncNetworkGraphFile'       : ''.join([networkName,'.png']),
                 'randomVariableCSVFile'     : ''.join([networkName,'RV.csv']),
                 }    
                
    ## if fileType=networkXmlFile check if master (dn = XXX) or simulated is meant
    if fileType == 'networkXmlFile':
        if dataNumber == 'xxx':
            fileType = ''.join([fileType,'XXX'])
        else:
            fileType = ''.join([fileType,'Sim'])
    
    ## find requested file name
    requestedFilename  = filenames[''.join([fileType])]
    ## find directory
    requestedDirectory = getDirectory(''.join([fileType,'Directory']), networkName, dataNumber, mode, exception)
    if requestedDirectory == None:
        if exception == "Warning":
            print("WARNING: moduleFilePathHandler.getFileAndPaths() directory of file '{}' does not exist. Exit()".format(requestedFilename))
            return None
        elif exception == "No":
            pass
        else:
            raise ValueError("ERROR: moduleFilePathHandler.getFileAndPaths() directory of file '{}' does not exist. Exit()".format(requestedFilename))
              
    ## requested file path 
    requestedFilePath = ''.join([requestedDirectory,'/',requestedFilename])
                
    if mode == 'read':    
        # if mode read
        ## ensure that the file exists
        if not os.path.isfile(requestedFilePath):
            if exception == "Warning":
                print("WARNING: moduleFilePathHandler.getFileAndPaths() file '{}' does not exist. Exit()".format(requestedFilePath))
                return None
            elif exception == "No":
                #print "raise no exception"
                return None
            else:
                raise ValueError("ERROR: moduleFilePathHandler.getFileAndPaths() file '{}' does not exist. Exit()".format(requestedFilePath))
              
    return requestedFilePath

# TODO: (einar) fix exception variable in function
def getDirectory(directoryType, networkName, dataNumber, mode, exception = 'Error'):
    """
    Function returns a requested directory path, if this directory does not exists
    it is created.

    directoryType:
        'randomVariableCSVFileDirectory'
        'workingDirectory',
        'configFileDirectory',
        'vesselCSVFileDirectory',
        'boundaryCSVFileDirectory',
        'networkXmlFileTemplateDirectory',
        'networkXmlFileXXXDirectory',
        'networkXmlFileSimDirectory',
        'networkXmlFileConvergenceDirectory',
        'solutionFileDirectory',
        'initialValueFileDirectory',
        'screenshotDirectory',
        'movieDirectory',
        'simulationDescriptionFileDirectory',
        'vncRescentNetworksFileDirectory',
        'vncNetworkGraphFileDirectory'


    networkName:

        name of the network file

    dataNumber:

        data number of solution file or xml file

    mode:

        read or write

    exception: (for read mode)

        Error (default): raise error and exit if file is not exiting
        Warning: just raise Warning and return with error string
    """
    
    existingDirectoryTypes = {
                              'workingDirectory',
                              'configFileDirectory',
                              'vesselCSVFileDirectory',
                              'boundaryCSVFileDirectory',
                              'networkXmlFileTemplateDirectory',
                              'networkXmlFileXXXDirectory',
                              'networkXmlFileSimDirectory',
                              'networkXmlFileConvergenceDirectory',
                              'solutionFileDirectory',
                              'solutionFileConvergenceDirectory', 
                              'networkLogFileDirectory',
                              'initialValueFileDirectory',
                              'screenshotDirectory',
                              'movieDirectory',
                              'simulationDescriptionFileDirectory',
                              'vncRescentNetworksFileDirectory',
                              'randomVariableCSVFileDirectory',
                              'vncNetworkGraphFileDirectory'} 
    
    if directoryType not in existingDirectoryTypes:
        raise ValueError("ERROR: getDirectory, requested directoryType {} is not in existingDirectoryTypes{}".format(directoryType, existingDirectoryTypes))
    
    ##definitions
    starfishHomeDirectory = module_path = os.path.dirname(starfish.__file__)
    

    workingDirectory = os.getcwd()

    if os.path.exists(os.path.join(os.getcwd(),STARFISHconfig)):
        configDirectory = os.getcwd()
    else:
        configDirectory = os.path.join(os.path.expanduser('~')) 
           
    networkXmlFileTemplateDirectory = ''.join([starfishHomeDirectory,'/TemplateNetworks/',networkName])
    networkXmlFileDirectory         = ''.join([workingDirectory,'/',networkName])
    networkXmlFileConvergenceDirectory         = ''.join([networkXmlFileDirectory,'/Convergence_001'])
    solutionFileDirectory           = ''.join([networkXmlFileDirectory,'/SolutionData_',str(dataNumber)])
    initialValueFileDirectory           = ''.join([networkXmlFileDirectory,'/InitialValues'])
    movieDirectory              = ''.join([solutionFileDirectory,'/Movies'])
    screenshotDirectory         = ''.join([solutionFileDirectory,'/Screenshots'])
    ## look up tables
    # directories
    directories = {
                   'workingDirectory'                   : workingDirectory,
                   'configFileDirectory'                : configDirectory,
                   'vesselCSVFileDirectory'             : networkXmlFileDirectory,
                   'boundaryCSVFileDirectory'           : networkXmlFileDirectory,
                   'randomVariableCSVFileDirectory'     : networkXmlFileDirectory,
                   'networkXmlFileTemplateDirectory'    : networkXmlFileTemplateDirectory,
                   'networkXmlFileXXXDirectory'         : networkXmlFileDirectory,
                   'networkXmlFileSimDirectory'         : solutionFileDirectory,
                   'networkXmlFileConvergenceDirectory' : networkXmlFileConvergenceDirectory,
                   'solutionFileConvergenceDirectory'   : networkXmlFileConvergenceDirectory,
                   'solutionFileDirectory'              : solutionFileDirectory,
                   'networkLogFileDirectory'            : solutionFileDirectory,
                   'initialValueFileDirectory'          : initialValueFileDirectory,
                   'simulationDescriptionFileDirectory' : networkXmlFileDirectory,
                   # 3d viz
                   'screenshotDirectory'                : screenshotDirectory,
                   'movieDirectory'                     : movieDirectory,
                   # vnc
                   'vncRescentNetworksFileDirectory'    : workingDirectory,
                   'vncNetworkGraphFileDirectory'       : networkXmlFileDirectory
                   }
    
    requestedDirectory = os.path.normpath(directories[directoryType])

    # if mode write
    if mode == 'write':
        ## ensure that the directory exists
        if not os.path.exists(requestedDirectory):
            os.makedirs(requestedDirectory)  
    if mode == 'read':
        if not os.path.exists(requestedDirectory):
            requestedDirectory = None
    
    return requestedDirectory

def createWorkingCopyOfTemplateNetwork(templateNetworkName, destinationNetworkName = None):
    """
    Function which copys all data from a template network wtih template network name into
    the working directory.
    It uses the same name of the template network if destinationNetworkName is not defined
    """
    if destinationNetworkName == None:
        destinationNetworkName = templateNetworkName.split('_template')[0]
    
    pathTemplateNetwork     = getDirectory('networkXmlFileTemplateDirectory', templateNetworkName, 'xxx', 'read')
    pathDestinationNetwork  = getDirectory('networkXmlFileXXXDirectory', destinationNetworkName, 'xxx', 'write')
    
    #loop through files
    for file in os.listdir(pathTemplateNetwork):
        # remove _template from name
        renamedFile = ''.join(file.split('_template'))
        # check if new name needs to be applied
        oldName = templateNetworkName.split('_template')[0]
        if oldName in renamedFile:
            renamedFile = ''.join([destinationNetworkName,renamedFile.split(oldName)[-1]])

        shutil.copy(os.path.join(*[pathTemplateNetwork,file]), os.path.join(*[pathDestinationNetwork,renamedFile]))
        
        newFilePath = os.path.join(*[pathDestinationNetwork,renamedFile])
        if ".xml" in newFilePath:
            setFlowFromFilePathToAbsolute(newFilePath, pathDestinationNetwork)

        
    return destinationNetworkName

    
def prettyPrintList(title, listToPrint, indexOffSet = 0):
    """
    Function to pretty print(a list to STDOUT with numbers to choose from)
    """
    print(title)
    for index,listElement in enumerate(listToPrint):
        print("   [ {:3} ] - {}".format(index+indexOffSet,listElement))

def userInputEvaluationInt(maxBound, minBound=0, question = "    insert your choice, (q)-quit: "):
    '''
    Question user to insert an integer number between minBound and maxBound
    '''
    appropriateInputList = [str(int(i+minBound)) for i in range(maxBound-minBound)]
    userInput = "NONE"
    appropriateInputList.append('q')
    print("")
    while userInput not in appropriateInputList:
        userInput =input3(question)
    print("")
    if userInput == 'q': exit()
    else: return int(userInput)

def updateSimulationDescriptions(networkName, currentDataNumber, currentDescription):
    """
    Function to update the text-file with the simulation description for the given network:
    Input:
        networkName <String> (= name of the network to process)
    
    workflow:
        1. open all pickle files and write out the simulation descriptions and datanumbers
        2. write information into file
    """
    # open File
    #simCaseDescFilePath = getFilePath('simulationDescriptionFile', networkName, currentDataNumber, 'read')#, exception = 'No')
    try:
        simCaseDescFilePath = getFilePath('simulationDescriptionFile', networkName, currentDataNumber, 'read', exception = 'No')
        simCaseDescFile = open(simCaseDescFilePath, 'r')
    except:
        simCaseDescFilePath = getFilePath('simulationDescriptionFile', networkName, currentDataNumber, 'write')#, exception = 'No')
    
        simCaseDescFile = open(simCaseDescFilePath, 'w+')
        simCaseDescFile.write("DataNumber   Description \n")
        simCaseDescFile.close()
        simCaseDescFile = open(simCaseDescFilePath, 'r')
            
    alreadyWritten = False
    # check if datanumber in file:
    simCaseDescriptionFileLines = simCaseDescFile.readlines()
    simCaseDescFile.close() 
    
    for Line in simCaseDescriptionFileLines:
        if currentDataNumber in Line: 
            if currentDescription not in Line or currentDescription == '':
                index = simCaseDescriptionFileLines.index(Line)
                simCaseDescriptionFileLines.remove(Line)
                simCaseDescriptionFileLines.insert(index,"  {} {} \n".format(currentDataNumber.ljust(10),currentDescription))
            alreadyWritten = True
            
    if alreadyWritten == False:
        simCaseDescriptionFileLines.append("  {} {} \n".format(currentDataNumber.ljust(10),currentDescription))
    
    #print simCaseDescriptionFileLines
    
    simCaseDescFile = open(simCaseDescFilePath, 'w')
    simCaseDescFile.writelines(simCaseDescriptionFileLines)
    simCaseDescFile.close()

def getSimulationCaseDescriptions(networkName, exception = 'Warning'):
    """
    TODO: review exception handling?
    """
    simCaseDescFilePath = getFilePath('simulationDescriptionFile', networkName, 'xxx', 'read', exception = 'No')
    try:
        simCaseDescFile = open(simCaseDescFilePath, 'r')
    except (FileNotFoundError, TypeError):
        net_dir=  getDirectory('networkXmlFileXXXDirectory', networkName, 'xxx', 'read', exception = 'No')
        solutions_exist = False
        for item in os.listdir(net_dir):
            if os.path.exists(os.path.join(item, item+'.xml')):
                solutions_exist = True
                break
        if not solutions_exist:
            return {}

        print('WARNING simulation description file of network {} does not exist!'.format(networkName))
        userInput = input3('Would you like to regenerate it? y/[n]')

        if userInput in ['y', 'Y', 'yes', 'Yes']:
            simCaseDescFilePath = getFilePath('simulationDescriptionFile', networkName, 'xxx', 'write', exception='No')
            # TODO should creation be handled in getFilePath?
            with open(simCaseDescFilePath, 'w') as simCaseDescFile:
                simCaseDescFile.write('')
            print('Regenerating simulation descriptions for {}'.format(networkName))
            regenerateSimulationCaseDescription(networkName)
        else:
            if exception == 'Warning':
                print("WARNING getSimulationCaseDescriptions() simulation description file of network {} does not exist!".format(networkName))
            elif exception == 'No':
                pass
            else:
                raise ValueError(exception)
            return {}

    with open(simCaseDescFilePath, 'r') as simCaseDescFile:
        simCaseDescriptionFileLines = simCaseDescFile.readlines()
    dataNumberDescriptionDict = {}
    for Line in simCaseDescriptionFileLines:
        sol = Line.split('{:8}'.format(' '))
        if len(sol)>1:
            dataNumberDescriptionDict[sol[0].split(' ')[-1]] = sol[1].split(' \n')[0]
    return dataNumberDescriptionDict


def regenerateSimulationCaseDescription(networkName):
    """
    Function to regenerate the simulation case descriptions of a network
    """
    # 0. get relevant path
    networkPath = getDirectory('networkXmlFileXXXDirectory', networkName, 'xxx', 'read', exception = 'No')
    # 1. get a list of existing solution directories
    simulations = glob.glob(networkPath +'/*_???')
    # 2. iterate through xml files and extract description
    for simulation in simulations:
        simulationName = os.path.basename(simulation)
        currentDataNumber = simulationName[-3::]
        networkXml = os.path.join(simulation, networkName + '_' + simulationName+".xml")
        try:
            data = etree.parse(networkXml)
            matches = data.xpath("//simulationContext/description")
            if len(matches) > 0:
                description = matches[0].text
            else:
                description = 'Not found in XML'
        except OSError:
            print('Could not open {}.'.format(networkXml))
            description = 'Missing XML file'
        updateSimulationDescriptions(networkName, currentDataNumber, description)

    
def loadExternalDataSet(fileName):
    """
    Function to open external (preprocessed) DataSets with the file ending *.v1dfExD
    Input:
        fileName <string> (with the total path)
    Output:
        extData <dict> 
    """
    externalData = {'PressureTime':None,'Pressure':None,'PressureUnit':'',
                    'FlowTime':None,'Flow':None,'FlowUnit':'',
                    'AreaTime':None,'Area':None,
                    'Description': ''}
    try:
        externalDataFile = open(fileName,'rb')
        externalData = pickle.load(externalDataFile)
    except:
        print("Error: no or corrupted external data-file found")
    
    return externalData

def setFlowFromFilePathToAbsolute(fileName, pathDestinationNetwork):
    """
    Function to change filePathName in Flow-FromFile xml element to absolute path after copying template network
    Input:
        fileName <string> (abs path of the copied xml fie)
        pathDestinationNetwork <string> (abs path of the directory of the xml file)
    Output:
        extData <dict> 
    """
    
    parser = etree.XMLParser(encoding='iso-8859-1')
    tree = etree.parse(fileName, parser)
    root = tree.getroot()
    for XmlElement in root:
        if XmlElement.tag == 'boundaryConditions':
            for vessel in XmlElement:
                for bc in vessel:
                    if bc.tag == 'Flow-FromFile':
                        for bcTag in bc:
                            if bcTag.tag == 'filePathName':
                                oldFilePath = bcTag.text
                                bcTag.text = os.path.join(pathDestinationNetwork, oldFilePath)
    tree.write(fileName)