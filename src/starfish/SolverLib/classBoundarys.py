import logging
logger = logging.getLogger(__name__)
import numpy as np
from starfish.UtilityLib import classStarfishBaseObject as cSBO
from starfish.NetworkLib import classBoundaryConditions as ccBC
MIN_NEG_PRESSURE_ALLOWED = -500*133.32
class Boundary(cSBO.StarfishBaseObject):
    def __init__(self, vessel, boundaryCondition, rigidArea, dt, currentMemoryIndex, currentTimeStep, nTsteps, systemEquation):
        """
        Constructor of Boundary
        s
        Initializes one Boundary of a vessel
        input: - boundaryConditions:
                  a list of all boundaryConditions of the vessel at one position i.g 0 or -1

        Note: if network consiats of a single vessel make sure the function is called twice
              once for the start and once for the end boundary at 0 and -1!

        variables (most important):
        self.position        :    position of the boundary
        self.ID              :    Vessel ID to which the boundary belongs
        self.duFunction      :    function which gives du-vector back
        self.bcType1         :    list of all type1 functions found in the given boundaryConditions
        self.omegaInput      :    function which gives the _omega of omega_ back depending on self.position
        self.omegaFunctio    :    function with boundaryCondtion of type2 calculating the omega-vector
        """

        self.position = boundaryCondition.position
        self.name = ' '.join(['Boundary',str(vessel.Id)])
        self.type = ''

        #Function which gives du-vector back
        self.duFunction = None
        self.duVector = np.zeros((nTsteps,2))
        #Function with boundaryCondtion of type2 calculating the omega-vector
        self.omegaFunction = None

        #System and Vessel Variables
        self.z  = vessel.z
        self.dt = dt
        self.currentTimeStep = currentTimeStep
        self.currentMemoryIndex = currentMemoryIndex
        self.systemEquation = systemEquation

        #SolutionVariables
        self.P = vessel.Psol
        self.Q = vessel.Qsol
        self.A = vessel.Asol

        self.BloodFlowSep  = np.zeros((nTsteps,2)) ## [ dbloodVolume in, dbloodVolume out] blood volume from one timestep to the next
        self.BloodVolumen  = np.zeros(2)

        bC = boundaryCondition
        self.bc = bC
        if bC.type == 1:
            if bC.conditionQuantity == "Pressure":
                bC.lastU = self.P[0,bC.position]
            else:
                bC.lastU = self.Q[0,bC.position]
            if not bC.runtimeEvaluation:
                self.duVector += np.diff(bC.calculateUVector(np.arange(nTsteps + 1) * dt), axis=0)

        #initialize solution variable
        self.BloodFlowSep[-1][self.position] = vessel.Qsol[0][self.position]

        self.type = self.bc.name
        if self.bc.type == 2:
            self.omegaFunction = self.bc
        else:
            if not self.bc.prescribeTotalValues:
                self.omegaFunction = ccBC.PrescribedInflux()
            elif self.bc.conditionQuantity == 'Flow':
                self.omegaFunction = ccBC.PrescribedTotalFlow()
            elif self.bc.conditionQuantity == 'Pressure':
                self.omegaFunction = ccBC.PrescribedTotalPressure()
            self.omegaFunction.setPosition(self.position)

        self.rigidArea = rigidArea
        self.A_nID = vessel.A_nID

    def __call__(self): #callMacCormackField(self):
        """
        new Boundary method calculates the values at the boundary
        and applies it to the new values
        """
        dt = self.dt
        currentMemoryIndex = self.currentMemoryIndex[0]
        currentTimeStep = self.currentTimeStep[0]
        currentSimTime = dt*currentTimeStep

        P = self.P[currentMemoryIndex]
        Q = self.Q[currentMemoryIndex]
        A = self.A[currentMemoryIndex]

        z = self.z
        position = self.position

        L,R,LMBD,Z1,Z2,_omega_field = self.systemEquation.updateLARL(P,Q,A,position)
        duPrescribed = None
        if self.bc.type == 1:
            if self.bc.runtimeEvaluation:
                if self.bc.prescribeTotalValues:
                    duPrescribed = self.bc.calculateU(currentSimTime + dt) - np.array([P[self.position], Q[self.position]])
                else:
                    duPrescribed = self.bc.calculateU(currentSimTime + dt) - self.bc.calculateU(currentSimTime)
            else:
                duPrescribed = self.duVector[currentTimeStep]
            Z = [Z1, -Z2] #TODO better way to handle sign
            if self.bc.conditionQuantity == 'Flow':
                duPrescribed[0]  = Z[self.position]*duPrescribed[1]
            elif  self.bc.conditionQuantity  == 'Pressure':
                duPrescribed[1]  = duPrescribed[0]/Z[self.position]
            else:
                raise ValueError('conditionQuanitity must be Flow or Pressure')


        # Call the actual function to determine incoming Riemann Invariant
        dPQ_calc,dBloodVolumen = self.omegaFunction(_omega_field, duPrescribed, R, L, currentMemoryIndex, currentTimeStep, dt, P[position], Q[position], A[position],Z1, Z2)

        # calculate new values for p and q
        P_calc = dPQ_calc[0]+ P[position]
        Q_calc = dPQ_calc[1]+ Q[position]

        # check new p value
        if P_calc < MIN_NEG_PRESSURE_ALLOWED:
            raise ValueError("{} calculated negative pressure P_calc = {} at time {} (n {},dt {})".format(self.name, P_calc, currentTimeStep*dt,currentTimeStep,dt))
            #exit()

        # calculate new value for the area
        A_calc = A[position] # assign old value
        if self.rigidArea == False:
            A_calc =  self.A_nID([P_calc],position)

        # apply values to solution array
        self.P[currentMemoryIndex+1][position] = P_calc
        self.Q[currentMemoryIndex+1][position] = Q_calc
        self.A[currentMemoryIndex+1][position] = A_calc

        try: self.BloodFlowSep[currentTimeStep] = self.BloodFlowSep[currentTimeStep-1]+dBloodVolumen
        except Exception: print("passed bloodflow integration")
        try:    self.BloodVolumen = self.BloodVolumen + 0.5*(self.BloodFlowSep[currentTimeStep-1]+self.BloodFlowSep[currentTimeStep])*dt
        except Exception: self.BloodVolumen = self.BloodVolumen + self.BloodFlowSep[currentTimeStep]*dt