#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import
from builtins import range
from future.utils import iteritems, iterkeys, viewkeys, viewitems, itervalues, viewvalues
from builtins import input as input3
import sys,os
cur = os.path.dirname(os.path.realpath(__file__))
#import progressbarsimple as cPB
import numpy as np
import chaospy as cp

from starfish.VascularPolynomialChaosLib.testBaseClass import TestBaseClass 
from starfish.VascularPolynomialChaosLib.classUqsaMeasures import UqsaMeasures
import starfish.VascularPolynomialChaosLib.chaospy_wrapper as cpw


class UqsaMethodPolynomialChaos(TestBaseClass):
    
    #----External Variables -------------------------------------------------------------#
    externVariables = {  'polynomialOrder' : TestBaseClass.ExtValue(int), 
                         'sampleFactor'    : TestBaseClass.ExtValue(int),
                         }
                
    externXmlAttributes  = []
    
    externXmlElements    = ['polynomialOrder', 
                            'sampleFactor']
    
    def __init__(self):
        '''
        Configuration class of a vascular polynomial chaos class
        '''             
        self.sampleFactor = 2
        #polynomialOrders of the polynomial chaos expansion || if more then one given they are processed consecutevely
        self.polynomialOrder = 3
                   
    def evaluateSamplesSize(self, distributionManager):
        '''
        function to evaluate the sample size
        '''
        # TODO anyway to do this simply without constructing the polynomials?
        # calculate samplesSize from expansion order
        orthoPoly = cp.generate_expansion(self.polynomialOrder, distributionManager.jointDistribution)
        samplesSize = self.sampleFactor * len(orthoPoly)
        abcSample = False
        return samplesSize,abcSample
                    
    def calculateOrthogonalPolynomials(self,distributionManager):
        '''
        Method to calculate orthogonal polynomials
        '''
        return cp.generate_expansion(self.polynomialOrder,distributionManager.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_gs(self.expansionOrder,self.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_chol(self.expansionOrder,self.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_svd(self.expansionOrder,self.jointDistribution)
  
    def calculateStatistics(self, distributionManager, sampleManager, qoi):

        sampleSize,abcSample     = self.evaluateSamplesSize(distributionManager)
        samples,samplesDependent = sampleManager.getSampleMatrices(sampleSize,abcSample)
        data                     = qoi.getData(sampleSize,abcSample)

        dependentCase = sampleManager.dependentCase
        confidenceAlpha = qoi.confidenceAlpha
        '''
        Function which calculates the gPCExpansion for the given data
        '''

        orthogonalPolynomials = self.calculateOrthogonalPolynomials(distributionManager)
        basis = cpw.generate_basis(self.polynomialOrder, distributionManager.jointDistribution)
        # polynomial chaos expansion
        gPCExpansion = cpw.fit_regression(basis, samples.T, data)


        # add this polynomial chaos method psudo spectral ...
        # q,w = cp.generate_quadrature(order, dist, rule="g") # 'g' just for independent; dependent use 'c'
        # gPCExpansion = cp.fit_quadrature(orth, nodes, weights, solves, retall, norms)


        # statistics
        stats = cpw.calc_descriptives(gPCExpansion)

        statsDict = {}

        statsDict['expectedValue']  = stats['mean']
        statsDict['variance']       = stats['variance']
        statsDict['standardDeviation']   = np.sqrt(statsDict['variance'])
        confidenceInterval  = cp.Perc(gPCExpansion['approx'], [confidenceAlpha/2., 100-confidenceAlpha/2.], distributionManager.jointDistribution)
        statsDict['confidenceInterval']  =  confidenceInterval.reshape(2,len(np.atleast_1d(statsDict['expectedValue'])))
        statsDict['confidenceAlpha'] = confidenceAlpha

        # conditional expected values  and sensitivity coefficients
        distributionDimension = len(distributionManager.jointDistribution)
        if distributionDimension > 1:
            # test dependecy or not
            if dependentCase == False:
                # independent case: use analytic expression from polynomial chaos expansion
                conditionalExpectedValue = []
                conditionalVariance      = []
                # conditional mean and variance
                for rvIndex in range(distributionDimension):
                    currDistMean = cp.E(distributionManager.jointDistribution)
                    currDistMean[rvIndex] = np.nan
                    # reduce polynomials
                    currPolynomTime = gPCExpansion['approx'](*currDistMean)
                    # TODO these are not so useful?
                    # conditionalExpectedValue.append(cp.E(currPolynomTime,distributionManager.jointDistribution))
                    # conditionalVariance.append(cp.Var(currPolynomTime,distributionManager.jointDistribution))

                    conditionalExpectedValue.append(np.zeros_like(currPolynomTime))
                    conditionalVariance.append(np.zeros_like(currPolynomTime))

                statsDict['conditionalExpectedValue'] = conditionalExpectedValue
                statsDict['conditionalVariance']      = conditionalVariance

                # sensitivity indices
                statsDict['firstOrderSensitivities'] = stats['sens_m'] #cp.Sens_m(gPCExpansion,distributionManager.jointDistribution)
                statsDict['totalSensitivities']      = stats['sens_t'] #cp.Sens_t(gPCExpansion,distributionManager.jointDistribution)
            else:
                # dependent rancom variables

                # this method is broken
                #sensindices = cp.Sens_nataf(distributionManager.expansionOrder, distributionManager.jointDistributionDependent, distributionManager.samplesDependent.T, self.data)
                #

                statsDict['firstOrderSensitivities'] = cp.Sens_m_nataf(self.polynomialOrder,
                                                               distributionManager.jointDistributionDependent,
                                                               samplesDependent.T,
                                                               data)

                statsDict['totalSensitivities']      = cp.Sens_t_nataf(self.polynomialOrder,
                                                               distributionManager.jointDistributionDependent,
                                                               samplesDependent.T,
                                                               data)

        statsDict['numberOfSamples'] = sampleSize
        # statistics
        uqsaMeasures = UqsaMeasures()
        uqsaMeasures.setVariablesDict(statsDict)
        return uqsaMeasures

class UqsaMethodPolynomialChaosPseudoSpectral(TestBaseClass):
    '''
    Configuration class of a vascular polynomial chaos class
    '''
    #----External Variables -------------------------------------------------------------#
    externVariables = {  'polynomialOrder' : TestBaseClass.ExtValue(int), 
                         'sampleFactor'    : TestBaseClass.ExtValue(int),
                         }
                
    externXmlAttributes  = []
    
    externXmlElements    = ['polynomialOrder', 
                            'sampleFactor']
    
    def __init__(self):
        '''
        
        '''                
        self.sampleFactor = 2
        #polynomialOrders of the polynomial chaos expansion || if more then one given they are processed consecutevely
        self.polynomialOrder = 3


    def evaluateSamplesSize(self, distributionManager):
        '''
        function to evaluate the sample size
        '''
        # TODO anyway to do this simply without constructing the polynomials?
        # calculate samplesSize from expansion order
        orthoPoly = cp.generate_expansion(self.polynomialOrder, distributionManager.jointDistribution)
        samplesSize = self.sampleFactor * len(orthoPoly)
        abcSample = False
        return samplesSize, abcSample


    def evaluateSamples(self,dist):
        #'g' just for independent; dependent use 'c'
        quatraturePoints,self.weights = cp.generate_quadrature(self.polynomialOrder, dist, rule="g")
        
                    
    def calculateOrthogonalPolynomials(self,distributionManager):
        '''
        Method to calculate orthogonal polynomials
        '''
        
        return cp.generate_expansion(self.polynomialOrder,distributionManager.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_gs(self.expansionOrder,self.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_chol(self.expansionOrder,self.jointDistribution)
        #self.orthogonalPolynomils = pc.orth_svd(self.expansionOrder,self.jointDistribution)
  
    def calculateStatistics(self, distributionManager, sampleManager, qoi):
        
        sampleSize,abcSample     = self.evaluateSamplesSize(distributionManager.distributionDimension)
        samples,samplesDependent = sampleManager.getSampleMatrices(sampleSize,abcSample)
        data                     = qoi.getData(sampleSize,abcSample)
        
        dependentCase = sampleManager.dependentCase
        confidenceAlpha = qoi.confidenceAlpha
        '''
        Function which calculates the gPCExpansion for the given data
        '''
                
        orthogonalPolynomials, norms = self.calculateOrthogonalPolynomials(distributionManager, retall = True)
        
        # add this polynomial chaos method psudo spectral ... # 
        gPCExpansion = cp.fit_quadrature(orthogonalPolynomials, samples, self.weights, data, norms)
        
        # statistics
        statsDict = {}
                     
        statsDict['expectedValue']  = cp.E(gPCExpansion, distributionManager.jointDistribution)
        statsDict['variance']       = cp.Var(gPCExpansion, distributionManager.jointDistribution)
        statsDict['standardDeviation']   = np.sqrt(statsDict['variance'])
        confidenceInterval  = cp.Perc(gPCExpansion, [confidenceAlpha/2., 100-confidenceAlpha/2.], distributionManager.jointDistribution)
        statsDict['confidenceInterval']  =  confidenceInterval.reshape(2,len(np.atleast_1d(statsDict['expectedValue'])))
        statsDict['confidenceAlpha'] = confidenceAlpha
        
        # conditional expected values  and sensitivity coefficients
        distributionDimension = len(distributionManager.jointDistribution)
        if distributionDimension > 1:
            # test dependecy or not
            if dependentCase == False:
                # independent case: use analytic expression from polynomial chaos expansion
                conditionalExpectedValue = []
                conditionalVariance      = []
                # conditional mean and variance
                for rvIndex in range(distributionDimension):
                    currDistMean = cp.E(distributionManager.jointDistribution)
                    currDistMean[rvIndex] = np.nan
                    # reduce polynomials
                    currPolynomTime = gPCExpansion(*currDistMean)
                    conditionalExpectedValue.append(cp.E(currPolynomTime,distributionManager.jointDistribution))
                    conditionalVariance.append(cp.Var(currPolynomTime,distributionManager.jointDistribution))    
                
                statsDict['conditionalExpectedValue'] = conditionalExpectedValue
                statsDict['conditionalVariance']      = conditionalVariance
                
                # sensitivity indices
                statsDict['firstOrderSensitivities'] = cp.Sens_m(gPCExpansion,distributionManager.jointDistribution)
                statsDict['totalSensitivities']      = cp.Sens_t(gPCExpansion,distributionManager.jointDistribution)
            else:
                # dependent rancom variables
                
                # this method is broken
                #sensindices = cp.Sens_nataf(distributionManager.expansionOrder, distributionManager.jointDistributionDependent, distributionManager.samplesDependent.T, self.data)
                #
                
                statsDict['firstOrderSensitivities'] = cp.Sens_m_nataf(self.polynomialOrder,
                                                               distributionManager.jointDistributionDependent,
                                                               samplesDependent.T,
                                                               data)
                
                statsDict['totalSensitivities']      = cp.Sens_t_nataf(self.polynomialOrder,
                                                               distributionManager.jointDistributionDependent,
                                                               samplesDependent.T,
                                                               data)
        
        statsDict['numberOfSamples'] = sampleSize
        # statistics
        uqsaMeasures = UqsaMeasures()
        uqsaMeasures.setVariablesDict(statsDict)
        return uqsaMeasures
  
class UqsaMethodMonteCarlo(TestBaseClass):
    '''
    Configuration class of a vascular polynomial chaos class
    '''
    #----External Variables -------------------------------------------------------------#
    externVariables = {  'sensitivityAnalysis'   : TestBaseClass.ExtValue(bool), 
                         'sampleSize'            : TestBaseClass.ExtValue(int)
                         }
                
    externXmlAttributes  = []
    
    externXmlElements    = ['sampleSize', 
                            'sensitivityAnalysis']
    
    def __init__(self):
        '''
        
        '''
        self.sampleSize = 10
        #
        self.sensitivityAnalysis = False
        
         
    def evaluateSamplesSize(self, distributionDimension):
        '''
        function to evaluate the sample size
        '''
        # calculate samplesSize from expansion order 
        abcSample = self.sensitivityAnalysis
        return self.sampleSize,abcSample
         
    def calculateStatistics(self, distributionManager, sampleManager, qoi):
        '''
        Function which calculates the gPCExpansion for the given data
        '''
                
        sampleSize,abcSample = self.evaluateSamplesSize(distributionManager.distributionDimension)
        
        samples,samplesDependent = sampleManager.getSampleMatrices(sampleSize,abcSample)
        data                     = qoi.getData(sampleSize,abcSample)
                
        dependentCase = sampleManager.dependentCase
        confidenceAlpha = qoi.confidenceAlpha
                
        statsDict = {}
        if dependentCase == False:
            
            
            # check if samples, and data have the right format
            distDim = distributionManager.distributionDimension
            if len(samples) != (distDim+2)*self.sampleSize:
                #print 'WARNING: Not simulated as sensitivity analysis case as len(samples) != (distDim+2)*self.sampleSize'
                self.sensitivityAnalysis = False
                
            if self.sensitivityAnalysis == False:
            
                statsDict['expectedValue']       = np.sum(data,axis=0)/len(samples)
                # variance = (data**2-mean)/len(samples)
                statsDict['variance']            = np.sum(data**2-statsDict['expectedValue']**2,axis=0)/len(samples)
                
                quantiles = [confidenceAlpha/2, 100.-confidenceAlpha/2]
                statsDict['confidenceInterval']  = np.percentile(data,quantiles, axis=0)
                statsDict['confidenceAlpha'] = confidenceAlpha
            else:
                print("Monte Carlo Sensitivity Analysis")  
                self.createSampleMatixHashTable(distDim)
                
                dataA = data[self.matrixHash['A'][0]:self.matrixHash['A'][1]]
                dataB = data[self.matrixHash['B'][0]:self.matrixHash['B'][1]]
                dataC = np.empty((distDim,self.sampleSize,len(data[0])))
                for d in range(distDim):
                    key = ''.join(['C',str(d)])
                    s = self.matrixHash[key][2]
                    e = self.matrixHash[key][3]
                    dataC[d] = data[s:e]
                
                meanA = np.sum(dataA,axis=0)/self.sampleSize
                meanB = np.sum(dataB,axis=0)/self.sampleSize
                #print np.shape(dataC), dataC
                #meanC = np.mean(dataC,axis= 0)
                #print np.shape(meanC), meanC
                mean = (meanA+meanB)/2.0
                dataAmm = dataA -mean
                dataBmm = dataB -mean
                dataCmm = dataC -mean
                # sensitivity
                f0sq = np.mean(dataAmm*dataBmm, axis=0)
                
                varianceA = np.sum(dataAmm**2,axis=0)/self.sampleSize - f0sq
                varianceB = np.sum(dataAmm**2,axis=0)/self.sampleSize - f0sq
            
                # conditional variance
                conditionalVarianceGivenQ = np.empty((distDim,len(data[0])))
                conditionalVarianceNotQ   = np.empty((distDim,len(data[0])))
            
                Si  =  []
                STi =  []
            
                for i in range(distDim):
                    conditionalVarianceGivenQ[i] = np.sum(dataAmm*dataCmm[i],axis=0)/self.sampleSize - f0sq
                    conditionalVarianceNotQ[i]   = np.sum(dataBmm*dataCmm[i],axis=0)/self.sampleSize - f0sq
                    Si.append(conditionalVarianceGivenQ[i]/(varianceA+(varianceA==0))*(varianceA!=0))
                    STi.append(1.- conditionalVarianceNotQ[i]/(varianceA+(varianceA==0))*(varianceA!=0))
                   
                statsDict['expectedValue']      = mean
                statsDict['variance']           = varianceA
                
                if varianceA.all() > 0:
                    statsDict['standardDeviation']  = np.sqrt(varianceA)
                
                quantiles = [confidenceAlpha/2, 100.-confidenceAlpha/2]
                statsDict['confidenceInterval'] = np.percentile(data,quantiles, axis=0) 
                statsDict['confidenceAlpha'] = confidenceAlpha
                
                statsDict['conditionalExpectedValue'] = None
                statsDict['conditionalVariance']      = np.array(conditionalVarianceGivenQ)
                
                # sensitivity indices
                statsDict['firstOrderSensitivities'] = np.array(Si)
                statsDict['totalSensitivities']      = np.array(STi)
        
        else:
            # TODO: implement dependent dist methods for MC
            raise NotImplementedError("NO MC methods for dependet distributions implemented")
        
        
        # statistics
        uqsaMeasures = UqsaMeasures()
        statsDict['numberOfSamples'] = sampleSize
        uqsaMeasures.setVariablesDict(statsDict)
        return uqsaMeasures
    
class UqsaMethodMonteCarloParametrizedBootstrapping(TestBaseClass):
    '''
    Configuration class of a 
    '''
    #----External Variables -------------------------------------------------------------#
    externVariables = {  'sensitivityAnalysis'   : TestBaseClass.ExtValue(bool), 
                         'sampleSize'            : TestBaseClass.ExtValue(int),
                         "chunkSize"             : TestBaseClass.ExtValue(int),
                         "averageNumber"         : TestBaseClass.ExtValue(int)
                         }
                
    externXmlAttributes  = []
    
    externXmlElements    = ['sampleSize', 
                            'sensitivityAnalysis',
                            "averageNumber",
                            "chunkSize"]
    
    def __init__(self):
        '''
        
        '''
        self.sampleSize = 10
        #
        self.sensitivityAnalysis = False
        # amount of averaged chunks 
        self.averageNumber = 10
        self.chunkSize = 100
         
    def evaluateSamplesSize(self, distributionDimension):
        '''
        function to evaluate the sample size
        '''
        # calculate samplesSize from expansion order 
        abcSample = self.sensitivityAnalysis
        return self.sampleSize+self.chunkSize*self.averageNumber,abcSample
         
    def calculateStatistics(self, distributionManager, sampleManager, qoi):
        '''
        Function which calculates the gPCExpansion for the given data
        '''
        statsDict = {}  
        sampleSize,abcSample = self.evaluateSamplesSize(distributionManager.distributionDimension)
        
        abcSample = self.sensitivityAnalysis
        sampleSize = self.sampleSize
        
        data                     = qoi.getData(1,abcSample, 0)
        expectedValue = np.empty((self.averageNumber,np.shape(data)[1]))
        variance      = np.empty((self.averageNumber,np.shape(data)[1]))
        
        for aNX in range(self.averageNumber):
            
                    
            offset = int(aNX * self.chunkSize) 
            
            samples,samplesDependent = sampleManager.getSampleMatrices(sampleSize,abcSample, offset)
            data                     = qoi.getData(sampleSize,abcSample, offset)
            
                    
            dependentCase = sampleManager.dependentCase
                
            if dependentCase == False:
                
                
                # check if samples, and data have the right format
                distDim = distributionManager.distributionDimension
                if len(samples) != (distDim+2)*self.sampleSize:
                    #print 'WARNING: Not simulated as sensitivity analysis case as len(samples) != (distDim+2)*self.sampleSize'
                    self.sensitivityAnalysis = False
                    
                if self.sensitivityAnalysis == False:
                
                    expectedValue[aNX] = np.sum(data,axis=0)/len(samples)
                    # variance = (data**2-mean)/len(samples)
                    variance[aNX] = np.sum(data**2-expectedValue[aNX]**2,axis=0)/len(samples)
                    
#                     quantiles = [confidenceAlpha/2, 100.-confidenceAlpha/2]
#                     statsDict['confidenceInterval']  = np.percentile(data,quantiles, axis=0)
#                     statsDict['confidenceAlpha'] = confidenceAlpha
                else:
                    
                    raise NotImplementedError("NO MC averaged methods for SA implemented")
            
            else:
                # TODO: implement dependent dist methods for MC
                raise NotImplementedError("NO MC methods for dependet distributions implemented")
        
        
        statsDict['expectedValue'] = expectedValue
        statsDict['variance'] = variance
        # statistics
        uqsaMeasures = UqsaMeasures()
        statsDict['numberOfSamples'] = sampleSize
        uqsaMeasures.setVariablesDict(statsDict)
        return uqsaMeasures
            
