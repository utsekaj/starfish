from __future__ import print_function, absolute_import
import os
import time
import multiprocessing
from functools import partial
import traceback
from starfish.SolverLib.class1DflowSolver import FlowSolver
from starfish.UtilityLib import moduleXML
import starfish.UtilityLib.moduleLogFile as mLOG
## module to run simulations as a batch jobs

def runBatchAsSingleProcess(batchDataList, quiet=False, writeLogFile=True):
    '''
    Run a set of simulations on one core without multiprocessing
    
    Args:
        batchDataList (list) :
            with data for each batch job [batchData1, batchData .. ]
        batchData (dict) :
             dict with {simulationIndex: , networkName: , dataNumber: , networkXmlFile: , pathSolutionDataFilename: }
        
    '''
    timeStartBatch = time.time()
    print('=====================================')
    print('------Single Process Batch Job-------')
    print('numberOfEval.:   {}'.format(len(batchDataList)))
    #progressBar = cPB.ProgressBar(35, len(batchDataList))
    for completed,batchData in enumerate(batchDataList):
        minutesSolve,secsSolve = runSingleBatchSimulation(batchData, writeLogFile=writeLogFile)
        if quiet == False:
            print('____________Batch   {:5} ___________'.format(batchDataList.index(batchData))) 
            print('Runtime:        {} min {} sec'.format(minutesSolve,secsSolve))
        #progressBar.progress()
    timeBatchJob= time.time()-timeStartBatch
    minutesBatch = int(timeBatchJob/60.)
    secsBatch = timeBatchJob-minutesBatch*60.
    print('=====================================')
    print('total runtime:  {} min {} sec'.format(minutesBatch,secsBatch))
    print('=====================================')
  
def runSingleBatchSimulation(batchData, writeLogFile=False):
    '''
    Function which runs a single simulation the defined with batchData
    
    Args:
        batchData (dict):
             dict with {simulationIndex: , networkName: , dataNumber: , networkXmlFile: , pathSolutionDataFilename: }
    '''
    networkName              = batchData['networkName']
    dataNumber               = batchData['dataNumber']
    networkXmlFileLoad       = batchData['networkXmlFileLoad']
    networkXmlFileSave       = batchData['networkXmlFileSave']
    pathSolutionDataFilename = batchData['pathSolutionDataFilename']
    try:
        timeStart = time.process_time() # TODO or perf_counter?
        vascularNetworkTemp = moduleXML.loadNetworkFromXML(networkName, dataNumber, networkXmlFile = networkXmlFileLoad, pathSolutionDataFilename = pathSolutionDataFilename)
        vascularNetworkTemp.quiet = True
        flowSolver = FlowSolver(vascularNetworkTemp, quiet=True)
        flowSolver.solve()
        vascularNetworkTemp.saveSolutionData()
        moduleXML.writeNetworkToXML(vascularNetworkTemp, dataNumber, networkXmlFileSave)
        
        if writeLogFile:
            networkLogFile = networkXmlFileSave.replace(".xml", '.tex')
            solutionFileDirectory = os.path.dirname(networkLogFile)
            mLog2 = mLOG.NetworkLogFile(vascularNetworkTemp, dataNumber=dataNumber,
                                        networkLogFile=networkLogFile, solutionFileDirectory=solutionFileDirectory, solutionFile=pathSolutionDataFilename
                                        , dt=flowSolver.dt, )
            mLog2.writeNetworkLogfile(compileLogFile=True, deleteAuxiliary=False) # TODO manage deleting aux files safely
        timeSolverSolve = time.process_time()-timeStart
        minutesSolve = int(timeSolverSolve/60.)
        secsSolve = timeSolverSolve-minutesSolve*60.
    except Exception as e:
        minutesSolve= 0
        secsSolve = 0
        print("Error in running {}".format(networkXmlFileLoad))
        print("The exception was: ")
        traceback.print_exc()
    return minutesSolve,secsSolve

def runBatchAsMultiprocessing(batchDataList, numberWorkers=None, quiet=False, writeLogFile=True):
    '''
    Run a set of simulations on one core without multiprocessing
    
    Args:
        batchDataList (list) :
            with data for each batch job [batchData1, batchData .. ]
        batchData (dict) :
             dict with {simulationIndex: , networkName: , dataNumber: , networkXmlFile: , pathSolutionDataFilename: }
    '''
    if numberWorkers == None:
        numberWorkers = multiprocessing.cpu_count()
    
    timeStartBatch = time.time()
    print('=====================================')
    print('------Multiprocessing Batch Job------')
    print('numberWorkers:   {}'.format(numberWorkers))
    print('numberOfEval.:   {}'.format(len(batchDataList)))

    pool = multiprocessing.Pool(numberWorkers, maxtasksperchild=None)
    part = partial(runSingleBatchSimulation, writeLogFile=writeLogFile)
    results = pool.imap(part, batchDataList)
    pool.close() 
    last_update = 0
    while (True):
        completed = results._index
        while last_update < completed:
            last_update += 1
        if (completed == len(batchDataList)): break
    pool.join()
    if quiet is False:
        print('=====================================')
        for batchJobIndex,[minutesSolve,secsSolve] in enumerate(results):
            print('____________Batch   {:5} ___________'.format(batchJobIndex+1)) 
            print('Runtime:        {} min {} sec'.format(minutesSolve,secsSolve))
        print('=====================================')
    timeBatchJob = time.time()-timeStartBatch
    minutesBatch = int(timeBatchJob/60.)
    secsBatch = timeBatchJob-minutesBatch*60.
    print("total runtime {:d} min {:.2f} sec".format(minutesBatch, secsBatch)) 
    print('=====================================')
