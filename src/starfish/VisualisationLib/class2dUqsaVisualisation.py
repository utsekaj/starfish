#!/usr/bin/env python
from __future__ import print_function, absolute_import
from builtins import range
from future.utils import iteritems, iterkeys, itervalues # TODO go to python3?
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
import matplotlib
matplotlib.use('GTK3Agg')
import matplotlib.pyplot as plt   
from matplotlib.figure import Figure
# uncomment to select /GTK/GTKAgg/GTKCairo
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
# from matplotlib.backends.backend_gtkcairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
from matplotlib.font_manager import FontProperties
from matplotlib.patches import Rectangle

import os
from copy import deepcopy as copy
import numpy as np

import starfish.VascularPolynomialChaosLib.moduleFilePathHandlerVPC as mFPH_VPC
import starfish.VascularPolynomialChaosLib.classDistributionManager as cDistMng
import starfish.VascularPolynomialChaosLib.classUqsaCase as cUqsaCase
from starfish.VascularPolynomialChaosLib.classUqsaMeasures import UqsaMeasures
import starfish.UtilityLib.moduleStartUp as mStartUp
import starfish.UtilityLib.moduleXML as mXML

import starfish.UtilityLib.processing as mProc
import starfish.UtilityLib.moduleStartUp as moduleStartUp
import starfish.UtilityLib.moduleFilePathHandler as mFPH
import starfish.UtilityLib.moduleXML as mXML

EXPAND=False
cur = os.path.dirname(os.path.realpath(__file__))


class Visualisation2DPlotWindowAdjustValues(gtk.Window):

    def __init__(self, parent, dictVariableName, actualValuesDict, inititalValuesDict, variableTypes=None,
                 variableFixedChoice=None):
        '''
        creates a window to update data written in a dictionary:

        dictVariableName    := <str> name of the dictionary in the parent class (used to update dict)
        actualValuesDict    := <dict> containing the data to be updated form {'variableName': [ item, item ...]}
        inititalValuesDict  := <dict> as actualValuesDict with initial values to reset initial values
        variableTypes       := <dict> as actualValuesDict but indicating the type of the variable which is used with e.g. 'str'
        variableFixedChoice := <dict> as actualValuesDict prescribe a fixed option for some variables, e.g. {'variableName': [ ['-',':'], ..]}

        for each item of each variable in the dictionary an entry element is created,
        if no type is defined (i.e. variableTypes == None) the variable is converted to float
        if a variableFixedChoice is defined for the variable a comboBox is created instead of a entry

        '''
        super(Visualisation2DPlotWindowAdjustValues, self).__init__()
        self.set_resizable(False)
        # self.set_position(gtk.WIN_POS_CENTER)

        self.parentWindow = parent
        self.dictVariableName = dictVariableName
        self.inititalValuesDict = copy(inititalValuesDict)
        self.actualValuesDict = actualValuesDict
        self.variableTypes = variableTypes
        self.variableFixedChoice = variableFixedChoice

        vbox = gtk.VBox(False, 1)

        buttonUpdate = gtk.Button("update")
        buttonUpdate.connect("clicked", self.on_clickedUpdate)
        buttonUpdate.set_size_request(120, 30)

        buttonReset = gtk.Button("reset")
        buttonReset.connect("clicked", self.on_clickedReset)
        buttonReset.set_size_request(120, 30)

        hbox = gtk.HBox(False, 10)
        hbox.pack_start(buttonUpdate, fill=False, expand=EXPAND, padding=0)
        hbox.pack_start(buttonReset, fill=False, expand=EXPAND, padding=0)
        vbox.pack_start(hbox, expand=EXPAND, fill=False, padding=0)

        self.entriesCombo = {}

        countHeight = 1
        countWidth = 1

        keys = list(actualValuesDict.keys())
        keys.sort()
        for key in keys:
            hbox = gtk.HBox(False, 10)
            label = gtk.Label(key)
            hbox.pack_start(label, fill=False, expand=EXPAND, padding=0)
            self.entriesCombo[key] = []

            countHeight = countHeight + 1

            values = actualValuesDict[key]
            countWidth = max(countWidth, len(values))
            for index, value in enumerate(values):
                # try to set up a combo box if defined
                # if not create a text entry field
                comboChoices = None
                try:
                    comboChoices = self.variableFixedChoice[key][index]
                except:
                    pass

                if comboChoices is not None:
                    entry = gtk.ComboBoxText()
                    for choice in comboChoices:
                        entry.append_text(choice)
                    entry.set_active(comboChoices.index(value))
                    entry.set_size_request(120, 30)
                    # cbType.connect("changed", self.on_changePlotType)
                else:
                    entry = gtk.Entry()
                    entry.set_size_request(120, 20)
                    # entry.add_events(gtk.gdk.KEY_RELEASE_MASK)
                    entry.set_text(str(value))

                hbox.pack_start(entry, fill=False, expand=False, padding=0)
                self.entriesCombo[key].append(entry)

            vbox.pack_start(hbox, expand=EXPAND, fill=False, padding=0)

        width = 130 + 120 * (countWidth)
        height = 30 + 30 * countHeight
        self.set_size_request(width, height)

        self.add(vbox)
        self.show_all()

    def on_clickedUpdate(self, widget):
        '''
        update dictionary from data
        written in entriesCombo and choosen from comboboxes
        '''
        for key, entries in iteritems(self.entriesCombo):
            for index, entry in enumerate(entries):

                comboChoices = None
                try:
                    comboChoices = self.variableFixedChoice[key][index]
                except:
                    pass

                if comboChoices is not None:
                    value = entry.get_active_text()
                    try:
                        type = self.variableTypes[key][index]
                    except:
                        type = 'str'
                    # TODO: Try Except Pass should be fixed
                    try:
                        self.actualValuesDict[key][index] = eval(type)(value)
                    except:
                        print("""WARNING: could not convert "{}" to "{}" """.format(text, type))

                else:
                    text = entry.get_text()
                    if text != '':
                        try:
                            type = self.variableTypes[key][index]
                        except:
                            type = 'float'
                        # TODO: Try Except Pass should be fixed
                        try:
                            self.actualValuesDict[key][index] = eval(type)(text)
                        except:
                            print("""WARNING: could not convert "{}" to "{}" """.format(text, type))

        self.parentWindow.update({self.dictVariableName: self.actualValuesDict})
        self.parentWindow.updatePlotWindow()

    def on_clickedReset(self, widget):
        '''
        Reset entriesCombo and and comboBoxes to entriesCombo of the beginning
        '''
        for key, entries in iteritems(self.entriesCombo):
            for index, entry in enumerate(entries):

                value = self.inititalValuesDict[key][index]

                comboChoices = None
                # TODO: Try Except Pass should be fixed
                try:
                    comboChoices = self.variableFixedChoice[key][index]
                except:
                    pass

                if comboChoices is not None:
                    valueIndex = comboChoices.index(value)
                    entry.set_active(valueIndex)
                else:
                    entry.set_text(str(value))

        self.parentWindow.update({self.dictVariableName: copy(self.inititalValuesDict)})
        self.parentWindow.updatePlotWindow()

class Visualisation2DPlotWindow(gtk.Window):
    def __init__(self, uqsaCase, plot_cases):
        self.plot = lambda: ''
        # # variables for the slider
        self.sliderValue = 0  # grid node / time point
        self.limitsWindow = None
        self.limits = {'Time': [0, 10],
                       'Space': [0, 10],
                       'gridNodes': [0, 5]}
        self.limitsInit = {}

        self.deltasWindow = None
        self.deltas = {}
        self.deltasInit = {}

        self.axis = {}
        self.lines = {}
        self.points = {}

        self.legend = None
        self.labelsWindow = None
        self.labels = {}
        self.labelsInit = {}
        self.labelsTypes = {}

        self.descriptions = None

        self.lineWindow = None
        self.lineProperties = {}
        self.linePropertiesInit = {}
        self.linePropertiesChoices = {}
        self.linePropertiesTypes = {}

        super(Visualisation2DPlotWindow, self).__init__()

        self.uqsaCase = uqsaCase
        self.plot_cases = plot_cases

        self.linewidth = 1.5
        self.fontSizeLabel = 14

        self.unitPtext = '$mmHg$'
        self.unitFtext = '$ml/s$'

        self.axisX = 'Time'
        self.scale = gtk.HScale()
        self.scale.set_range(0, 10)
        self.scale.set_increments(1, 1)
        self.scale.set_digits(0)
        self.scale.set_size_request(400, 30)
        # self.scale.connect('value-changed', self.on_changedSlider)
        self.scale.set_draw_value(False)

        self.nodeLabel = gtk.Label('Node 0')

        # # image legend
        self.cBlegend = gtk.CheckButton("show legend")
        # self.cBlegend.connect("toggled", self.on_changedLegend)
        self.cBlegend.set_size_request(120, 30)

        # medical units
        #         self.medicalUnit = True
        #         cBmedical = gtk.CheckButton("use medical units")
        #         #cBmedical.connect("toggled", self.on_changedMedicalBool)
        #         cBmedical.set_size_request(120,30)
        #         cBmedical.set_active(1)

        # show decriptions
        self.buttonDescription = gtk.CheckButton('show descriptions')
        # self.buttonDescription.connect("toggled", self.on_changedDescriptions)
        self.buttonDescription.set_size_request(120, 30)
        # render movie button
        self.buttonRenderMovie = gtk.Button("render movie")
        # self.buttonRenderMovie.connect("clicked", self.on_clickedRenderMovie)
        self.buttonRenderMovie.set_size_request(120, 30)
        self.buttonRenderMovie.set_sensitive(False)

        #### second row of buttons
        # min max plots
        self.buttonMinMaxPoints = gtk.ToggleButton('MinMax points')
        # self.buttonMinMaxPoints.connect("toggled", self.on_changedPlotMinMax)
        self.buttonMinMaxPoints.set_size_request(120, 30)

        buttonDeltas = gtk.ToggleButton("update Deltas")
        # buttonDeltas.connect("toggled", self.on_changeDeltas)
        buttonDeltas.set_size_request(120, 30)

        # checkInflation = gtk.CheckButton('Point of Inflection')
        # checkInflation.connect("toggled", self.on_changedDescription)
        # checkInflation.set_size_request(120, 30)

        #         cBchangeMinMaxDelta = gtk.Button('change MinMax-deltas')
        #         cBchangeMinMaxDelta.connect("clicked", self.on_clickedMinMaxDelta)
        #         cBchangeMinMaxDelta.set_size_request(120,30)
        #         cBchangeMinMaxDelta.set_sensitive(False)

        buttonLimits = gtk.ToggleButton("update limits")
        #buttonLimits.connect("toggled", self.on_changeLimits)
        buttonLimits.set_size_request(120, 30)

        buttonLabels = gtk.ToggleButton("update labels")
        #buttonLabels.connect("toggled", self.on_changeLabels)
        buttonLabels.set_size_request(120, 30)

        self.buttonLines = gtk.ToggleButton("update lines")
        #self.buttonLines.connect("toggled", self.on_changeLines)
        self.buttonLines.set_size_request(120, 30)

        self.fig = Figure(figsize=(5, 4), dpi=100)
        # self.plot(0)
        self.canvas = FigureCanvas(self.fig)  # a gtk.DrawingArea
        width = 640
        height = 500  # 690
        self.canvas.set_size_request(width, height)

        toolbar = NavigationToolbar(self.canvas, self)
        self.active_plot_idx = 0
        cbType = gtk.ComboBoxText()
        cbTypeFcnHandles = []
        cbType.append_text('Plot Mean and Variance')
        cbTypeFcnHandles.append(self.updateMeanVar)
        cbType.append_text('First and Total')
        cbTypeFcnHandles.append(self.updateFirstTotal)
        cbType.append_text('Plot Mean and First')
        cbTypeFcnHandles.append(self.updateMeanFirst)
        cbType.append_text('Plot Mean and Total')
        cbTypeFcnHandles.append(self.updateMeanTotal)
        cbType.append_text('Plot Time Averaged First')
        cbTypeFcnHandles.append(self.updateTavgFirst)
        cbType.append_text('Plot Time Averaged Total')
        cbTypeFcnHandles.append(self.updateTavgTotal)
        cbType.set_active(self.active_plot_idx)
        cbType.connect("changed", self.on_changePlotType)
        self.cbTypeFcnHandles = cbTypeFcnHandles

        self.cbQuantity = gtk.ComboBoxText()
        self.cbQuantityVals = []
        qloc = self.plot_cases[0].currentVesselId # TODO deal with multiple locations?
        loc = [l for l in uqsaCase.locationOfInterestManager.locationsOfInterest.values() if l.queryLocation == qloc][0]
        for qoi_type, qoi in loc.quantitiesOfInterest.items():
            self.cbQuantity.append_text(qoi_type)
            self.cbQuantityVals.append(qoi_type)

        self.cbQuantity.set_active(0)
        self.cbQuantity.connect("changed", self.on_changeQTY)
        self.active_qoi = self.cbQuantityVals[0]

        self.cbRandomVar = gtk.ComboBoxText()
        self.cbRandomVar.append_text('All')
        self.cbRandomVarVals = ['All',]
        for rv in self.uqsaCase.sampleManager.randomVariableNames:
            self.cbRandomVar.append_text(rv)
            self.cbRandomVarVals.append(rv)
        self.cbRandomVar.set_active(0)
        self.cbRandomVar.connect("changed", self.on_changeRV)
        self.active_rv = self.cbRandomVarVals[0]
        self.nRandom = len(self.cbRandomVarVals)-1

        vbox = gtk.VBox(False, 1)
        hboxLegend = gtk.HBox(False, 10)
        hboxCheckboxes2 = gtk.HBox(False, 10)
        hbox = gtk.HBox(False, 1)
        hbox2 = gtk.HBox(False, 1)

        # Checkbutton series
        hboxLegend.pack_start(self.cBlegend, expand=EXPAND, fill=True, padding=0)
        hboxLegend.pack_start(buttonLabels, expand=EXPAND, fill=True, padding=0)
        hboxLegend.pack_start(self.buttonDescription, expand=EXPAND, fill=True, padding=0)

        # hboxLegend.pack_start(cBmedical)
        hboxLegend.pack_start(self.buttonRenderMovie, expand=EXPAND, fill=True, padding=0)
        hboxCheckboxes2.pack_start(self.buttonMinMaxPoints, expand=EXPAND, fill=True, padding=0)
        hboxCheckboxes2.pack_start(buttonDeltas, expand=EXPAND, fill=True, padding=0)
        hboxCheckboxes2.pack_start(buttonLimits, expand=EXPAND, fill=True, padding=0)
        hboxCheckboxes2.pack_start(self.buttonLines, expand=EXPAND, fill=True, padding=0)

        # align pictures canvas
        alignIm = gtk.Alignment()
        alignIm.set(0, 1, 1, 0)
        alignIm.add(self.canvas)
        # align node switcher scale
        hbox.pack_start(self.nodeLabel, expand=EXPAND, fill=True, padding=0)
        hbox.pack_start(self.scale, expand=EXPAND, fill=True, padding=0)
        alignHbox = gtk.Alignment()
        alignHbox.set(0, 1, 1, 0)
        alignHbox.add(hbox)
        # align combobox
        hbox2.pack_start(cbType, expand=EXPAND, fill=True, padding=0)
        hbox2.pack_start(self.cbQuantity, expand=EXPAND, fill=True, padding=0)
        hbox2.pack_start(self.cbRandomVar, expand=EXPAND, fill=True, padding=0)

        alignCB = gtk.Alignment()
        alignCB.set(0, 1, 1, 0)
        alignCB.add(hbox2)
        # align navigation toolbox
        alignNT = gtk.Alignment()
        alignNT.set(0, 1, 1, 0)
        alignNT.add(toolbar)

        # put all together
        vbox.pack_start(hboxLegend, expand=EXPAND, fill=True, padding=0)
        vbox.pack_start(hboxCheckboxes2, expand=EXPAND, fill=True, padding=0)
        vbox.pack_start(alignCB, expand=EXPAND, fill=True, padding=0)
        vbox.pack_start(alignNT, expand=EXPAND, fill=True, padding=0)
        vbox.pack_start(alignIm, expand=EXPAND, fill=True, padding=0)
        vbox.pack_start(alignHbox, expand=EXPAND, fill=True, padding=0)

        self.add(vbox)
        self.show_all()

        self.createGraph()
        self.estimatePlotLimits()
        self.plot = self.updateMeanVar

        # # update slider and so --> update the plot
        # self.scale.set_range(*self.limits['gridNodes'])
        # TODO enable inlet/outlet
        self.scale.set_range(-1, self.limits['gridNodes'][-1])
        self.updatePlotWindow()
        self.connect('check-resize', self.on_resize)

    def update(self, visualisationData):
        '''
        updates the vascularNetwork data using a dictionary in form of
        visualisationData = {'dictVariableName': value}
        '''
        for key, value in iteritems(visualisationData):
            try:
                self.__getattribute__(key)
                self.__setattr__(key, value)
            except:
                print('WARNING vascularNetwork.update(): wrong key: %s, could not update vascularNetwork' % key)

    def createGraph(self):
        '''
        create graph with 2 subplots and all necessary lines
        '''
        self.fig = plt.figure(figsize=(6, 6), dpi=100, edgecolor='k') # TODO resize with gtk figure
        self.fig.subplots_adjust(right=0.86)
        self.fig.subplots_adjust(left=0.17)
        self.fig.subplots_adjust(top=0.95)
        self.fig.subplots_adjust(bottom=0.15)
        self.fig.subplots_adjust(hspace=0.18)

        fontLegend = FontProperties()
        fontLegend.set_size(self.fontSizeLabel)

        from matplotlib import rc
        from matplotlib import rcParams

        rcParams['text.usetex'] = False
        #        rcParams['text.latex.unicode'] = True
        rcParams['font.family'] = 'sans-serif'
        rcParams['font.size'] = self.fontSizeLabel
        rcParams['savefig.dpi'] = 300.

        ax1 = plt.subplot(2, 1, 1, frameon=True)
        plt.xticks(np.linspace(ax1.get_xlim()[0], ax1.get_xlim()[1], 2), ['', ''])
        ax1.tick_params(axis='x', top='off', bottom='off')
        ax1.spines['bottom'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        ax1.tick_params(axis='y', right='off')

        # plt.yticks(np.linspace(ax12.get_xlim()[0], ax12.get_xlim()[1], 2), ['', ''])

        ax2 = plt.subplot(2, 1, 2, frameon=True)
        ax2.spines['top'].set_visible(False)
        ax2.tick_params(axis='x', top='off')
        ax2.spines['right'].set_visible(False)
        ax2.tick_params(axis='y', right='off')

        ax2.set_xlabel('Time $s$', fontsize=self.fontSizeLabel)


        # plt.yticks(np.linspace(ax22.get_xlim()[0], ax22.get_xlim()[1], 2), ['', ''])

        self.axis = {'axis1': ax1, 'axis2': ax2}
        self.case_linestyle = ['-', '--', ':']
        # # add 3 lines for each network case to each subplot
        colors = ['b', 'r', 'm', 'g', 'c', 'k']
        linestyles = {'axis1': ['-'],  'axis2': ['-']}

    def clearPlotWindow(self, lines=True, points=True, labels=False):
        '''
        set all line data to single point at (-1, 0) out of the view field
        '''
        for caseId, axisDict in iteritems(self.lines):
            for axis, lineDict in iteritems(axisDict):
                if lines:
                    for line in itervalues(lineDict):
                        line.set_data([-1], [0])
                    self.axis['axis1'].spines['right'].set_visible(False)
                    self.axis['axis2'].spines['right'].set_visible(False)
                if points:
                    try:
                        for point in itervalues(self.points[caseId][axis]):
                            point.set_data([-1], [0])
                    except Exception:
                        pass
                if labels:
                    for linestyle in iterkeys(lineDict):
                        currentName = ' '.join([str(caseId), axis, linestyle])
                        self.labels[currentName] = ''

        self.canvas.figure = self.fig
        self.fig.set_canvas(self.canvas)
        self.canvas.queue_resize()

    def updatePlotWindow(self):
        '''
        update the plot canvas in the GUI
        '''
        self.plot()

        if self.buttonMinMaxPoints.get_active():
            self.updatePoints()

        if self.cBlegend.get_active():
            self.updateLegend()
            try:
                self.legend.set_visible(True)
            except:
                pass
        else:
            try:
                self.legend.set_visible(False)
            except:
                pass

        if self.buttonLines.get_active():
            self.updateLineProperties()

        if self.buttonDescription.get_active():

            self.updateDescriptions()
            self.descriptions.set_visible(True)
        else:
            # TODO: Try Except Pass should be fixed
            try:
                self.descriptions.set_visible(False)
            except:
                pass

        self.canvas.figure = self.fig
        self.fig.set_canvas(self.canvas)
        self.canvas.queue_resize()

    def updateLegend(self):
        '''
        Stores the labels of the lines in the legend
        '''
        handles = []
        labels = []
        for axis in itervalues(self.axis):
            ha, la = axis.get_legend_handles_labels()
            for currentAxis, currentLine in zip(ha, la):
                label = self.labels[currentLine][0]
                if label != '':
                    labels.append(label)
                    handles.append(currentAxis)
        # TODO: Try Except Pass should be fixed
        try:
            self.legend.set_visible(False)
            self.legend.remove()
        except:
            pass
        self.legend = self.fig.legend(handles, labels, loc=3, borderaxespad=0., frameon=False,
                                      fontsize=self.fontSizeLabel / 4. * 3.)

    def updateDescriptions(self):
        '''
        Stores the descriptions of the cases in the legend
        '''
        handles = []
        labels = []
        for caseId, vascularNetwork in enumerate(self.selectedNetworks):
            currentColor = self.lines[caseId]['axis1']['-'].get_color()
            feakLine = Rectangle((-1, -1.005), -1.005, -1.005, fc=currentColor, fill=True, edgecolor='none',
                                 linewidth=0)
            handles.append(feakLine)
            labels.append(vascularNetwork.description)
        # TODO: Try Except Pass should be fixed
        try:
            self.descriptions.set_visible(False)
            self.descriptions.remove()
        except:
            pass

        self.descriptions = self.fig.legend(handles, labels, loc=4, borderaxespad=0., frameon=False,
                                            fontsize=self.fontSizeLabel / 4. * 3.)

    def updateLineProperties(self):
        '''
        updates line informations:

           color
           linestyle
           linewidth
        '''
        for caseId, axisDict in iteritems(self.lines):
            for axis, lineDict in iteritems(axisDict):
                for linestyle, line in iteritems(lineDict):
                    currentName = ' '.join([str(caseId), axis, linestyle])
                    color, style, width = self.lineProperties[currentName]
                    line.set_color(color)
                    line.set_linestyle(style)
                    line.set_linewidth(width)

    def updateMeanVar(self):
        '''
        create line plot with P in the first axis and Q the second axis
        '''
        gridNode = self.sliderValue
        # 1. set axis label

        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'E({self.active_qoi})' + self.unitPtext, fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'Std({self.active_qoi})' + self.unitFtext, fontsize=self.fontSizeLabel)
        qoi = self.active_qoi
        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if l.queryLocation == qloc][0]
            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            expected_value = uqsaMeasures.expectedValue
            prediction_interval = uqsaMeasures.confidenceInterval

            ax = self.axis['axis1']
            ax.plot(xVals, expected_value, color=case_color, label='E[-]')
            ax.fill_between(xVals, prediction_interval[0],
                            prediction_interval[1], alpha=0.3, color=case_color)
            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Pressure [mmHg]')

            ax = self.axis['axis2']
            ax.plot(xVals, uqsaMeasures.standardDeviation, color=case_color, label='Std[-]')
            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Std(Pressure) [mmHg]')

    def updateFirstTotal(self):
        qoi = self.active_qoi
        rv = self.active_rv
        rv_idx = self.cbRandomVarVals.index(rv)-1
        if rv_idx < 0:
            rv_idx = list(range(len(self.cbRandomVarVals)-1))
        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'$S_{{M}}$({rv}, {self.active_qoi})', fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'$S_{{T}}$({rv}, {self.active_qoi})', fontsize=self.fontSizeLabel)

        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if
                   l.queryLocation == qloc][0]
            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            expected_value = uqsaMeasures.expectedValue
            prediction_interval = uqsaMeasures.confidenceInterval
            Sm = uqsaMeasures.firstOrderSensitivities[rv_idx]
            St = uqsaMeasures.totalSensitivities[rv_idx]
            ax = self.axis['axis1']
            lm = ax.plot(xVals, Sm.T, '--', alpha=0.5)
            ax.set_ylim((0,1.1))
            lt = ax.plot(xVals, St.T,)
            for m, t in zip(lm,lt):
                t.set_color(m.get_color())
            ax.set_xlabel('Time [s]')
            ax.set_ylim((0,1.1))


    def updateMeanFirst(self):
        rv = self.active_rv
        rv_idx = self.cbRandomVarVals.index(rv)-1
        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'E({self.active_qoi})' + self.unitPtext, fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'$S_{{m}}$({rv}, {self.active_qoi})', fontsize=self.fontSizeLabel)
        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if
                   l.queryLocation == qloc][0]
            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            expected_value = uqsaMeasures.expectedValue
            prediction_interval = uqsaMeasures.confidenceInterval
            ax = self.axis['axis1']
            ax.plot(xVals, expected_value, color=case_color, label='E[-]')
            ax.fill_between(xVals, prediction_interval[0],
                            prediction_interval[1], alpha=0.3, color=case_color)
            ax.set_ylabel('Pressure [mmHg]')
            ax = self.axis['axis2']
            Sm = uqsaMeasures.firstOrderSensitivities
            if rv_idx < 0:
                ax.plot(xVals, Sm.T, self.case_linestyle[case_idx])
            else:
                ax.plot(xVals, Sm[rv_idx], color=case_color)
            ax.set_ylim((0,1.1))

    def updateTavgFirst(self):
        rv = self.active_rv
        rv_idx = self.cbRandomVarVals.index(rv)-1
        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'E({self.active_qoi})' + self.unitPtext, fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'$S_{{m}}$({rv}, {self.active_qoi})', fontsize=self.fontSizeLabel)
        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if
                   l.queryLocation == qloc][0]
            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            variance = uqsaMeasures.variance
            Sm = uqsaMeasures.firstOrderSensitivities
            TaSm = np.mean(Sm*variance, axis=1)/np.mean(variance)
            ax = self.axis['axis1']
            bars = ax.bar(np.arange(self.nRandom), TaSm, tick_label=self.cbRandomVarVals[1:],
                          color=[f'C{idx}' for idx in range(self.nRandom) ])
            # ax.bar(np.arange(len(self.cbRandomVarVals)), TaSm, tick_label=self.cbRandomVarVals)
            ax.set_ylabel('Pressure [mmHg]')
            ax = self.axis['axis2']
            if rv_idx < 0:
                ax.plot(xVals, Sm.T, self.case_linestyle[case_idx])
            else:
                ax.plot(xVals, Sm[rv_idx], color=case_color)
            ax.set_ylim((0,1.1))

    def updateTavgTotal(self):
        rv = self.active_rv
        nrv = self.nRandom
        rv_idx = self.cbRandomVarVals.index(rv)-1
        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'E({self.active_qoi})' + self.unitPtext, fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'$S_{{m}}$({rv}, {self.active_qoi})', fontsize=self.fontSizeLabel)
        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if
                   l.queryLocation == qloc][0]
            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            variance = uqsaMeasures.variance
            St = uqsaMeasures.totalSensitivities
            TaSt = np.mean(St*variance, axis=1)/np.mean(variance)
            ax = self.axis['axis1']
            bars = ax.bar(np.arange(nrv), TaSt, tick_label=self.cbRandomVarVals[1:],
                          color=[f'C{idx}' for idx in range(nrv) ])
            ax.set_ylabel('Pressure [mmHg]')
            ax = self.axis['axis2']

            if rv_idx < 0:
                ax.plot(xVals, St.T, self.case_linestyle[case_idx])
            else:
                ax.plot(xVals, St[rv_idx], color=case_color)
            ax.set_ylim((0,1.1))

    def updateMeanTotal(self):
        rv = self.active_rv
        rv_idx = self.cbRandomVarVals.index(rv)-1
        self.axis['axis1'].clear()
        self.axis['axis2'].clear()
        self.axis['axis1'].set_ylabel(f'E({self.active_qoi})' + self.unitPtext, fontsize=self.fontSizeLabel)
        self.axis['axis2'].set_ylabel(f'$S_{{T}}$({rv}, {self.active_qoi})',
                                      fontsize=self.fontSizeLabel)
        xVals = self.uqsaCase.locationOfInterestManager.simulationTime
        for case_idx, case in enumerate(self.plot_cases):
            case_color = f'C{case_idx}'
            method = case.current_method
            qloc = case.currentVesselId
            loc = [l for l in self.uqsaCase.locationOfInterestManager.locationsOfInterest.values() if
                   l.queryLocation == qloc][0]

            qoi = loc.quantitiesOfInterest[self.active_qoi]
            uqsaMeasures = qoi.uqsaMeasures[method]
            expected_value = uqsaMeasures.expectedValue
            prediction_interval = uqsaMeasures.confidenceInterval

            ax = self.axis['axis1']
            ax.plot(xVals, expected_value, color=case_color, label='E[-]')
            ax.fill_between(xVals, prediction_interval[0],
                            prediction_interval[1], alpha=0.3, color=case_color)
            ax.set_ylabel('Pressure [mmHg]')

            ax = self.axis['axis2']
            St = uqsaMeasures.totalSensitivities
            if rv_idx<0:
                ax.plot(xVals, St.T, self.case_linestyle[case_idx])
            else:
                ax.plot(xVals, St[rv_idx], color=case_color)
            ax.set_ylim((0,1.1))

    def estimatePlotLimits(self):
        '''
        This function evaluates all limits for the plots
        It may take a while to calculate all
        '''
        self.limits = {'P': [1e50, -1e50],
                       'Pf': [1e50, -1e50],
                       'Pb': [1e50, -1e50],
                       'Pfb': [1e50, -1e50],
                       'PfbLev': [1e50, -1e50],
                       'Q': [1e50, -1e50],
                       'Qf': [1e50, -1e50],
                       'Qb': [1e50, -1e50],
                       'Qfb': [1e50, -1e50],
                       'QfbLev': [1e50, -1e50],
                       'Time': [0, -1e50],
                       'Space': [0, -1e50],
                       'c': [1e50, -1e50],
                       'CFL': [0, 1.1],
                       'A': [1e50, -1e50],
                       'C': [1e50, -1e50],
                       'gridNodes': [0, -1e50],
                       'G': [-0.25, 0.25]}

        self.limitsInit = copy(self.limits)

    def on_changePlotType(self, widget):
        print('change plot type')
        self.clearPlotWindow()
        cbIndex = widget.get_active()
        self.active_plot_idx = cbIndex
        self.plot = self.cbTypeFcnHandles[cbIndex]
        self.plot()
        self.canvas.draw_idle()

    def on_changeQTY(self, widget):
        self.active_qoi = self.cbQuantityVals[widget.get_active()]
        self.plot = self.cbTypeFcnHandles[self.active_plot_idx]
        print('change QOI', self.active_qoi)
        self.plot()
        self.canvas.draw_idle()

    def on_changeRV(self, widget):
        self.active_rv = self.cbRandomVarVals[widget.get_active()]
        print('change RV', self.active_rv)
        self.plot = self.cbTypeFcnHandles[self.active_plot_idx]
        self.plot()
        self.canvas.draw_idle()

    def on_resize(self, widget):
        window_size = self.get_size()
        width = window_size[0]
        height = int(0.8*window_size[1])
        if width < 600:
            width = 600
        if height < 500:
            height = 500
        self.canvas.set_size_request(width, height)

class Visualisation2DMainCase(object):
    def __init__(self, number):
        
        self.networkInfo = {"choose simulation case" : [[], '-'] }
        self.uqsaMethods = ["choose simulation case"]
        self.current_method = self.uqsaMethods[0]
        self.currentVesselId = None
                
        # description of the dataSet
        self.networkDescription = gtk.Label(' here is the description that can be rather long')
        self.networkDescription.set_size_request(400, 35)
        # # Combo boxes
        # vessel chooser
        self.comboBoxVessels = gtk.ComboBoxText()
        self.comboBoxVessels.append_text("choose vessel")
        self.comboBoxVessels.connect("changed", self.on_changedCBvessels) 
        self.comboBoxVessels.set_active(0)

        # Network SolutionData chooser
        self.comboBoxNetworks = gtk.ComboBoxText()
        self.comboBoxNetworks.append_text("choose simulation case")
        self.comboBoxNetworks.connect("changed", self.on_changedNetworkComboBox) 
        self.comboBoxNetworks.set_size_request(250, 35)
        self.comboBoxNetworks.set_active(0)    
        textComboBoxNetworks = gtk.Label(' '.join(['Case', str(number)]))
        textComboBoxNetworks.set_size_request(100, 35)             
        
        # Case
        self.vBoxCase = gtk.VBox(False, 10)
        hBoxButtons = gtk.HBox(False, 10)
        hBoxDecription = gtk.HBox(False, 10)

        spacingText25Box = gtk.Label('')
        spacingText25Box.set_size_request(5, 35)
        spacingText25BoxDesc = gtk.Label('')
        spacingText25BoxDesc.set_size_request(25, 35)

        hBoxButtons.pack_start(spacingText25Box, fill=False, expand=False, padding=0)
        hBoxButtons.pack_start(textComboBoxNetworks, fill=False, expand=False, padding=0)
        hBoxButtons.pack_start(self.comboBoxNetworks, fill=False, expand=False, padding=0)
        hBoxButtons.pack_start(self.comboBoxVessels, fill=False , expand=False, padding=0)
         
        hBoxDecription.pack_start(spacingText25BoxDesc, fill=False, expand=False, padding=0)
        hBoxDecription.pack_start(self.networkDescription, fill=False, expand=False, padding=0)

        separator = gtk.HSeparator()
        
        self.vBoxCase.pack_start(hBoxButtons, expand=True, fill=False, padding=0)
        self.vBoxCase.pack_start(hBoxDecription, expand=True, fill=False, padding=0)
        self.vBoxCase.pack_start(separator, expand=True, fill=False, padding=0)
        
    def updateVesselComboBox(self):
        '''
        Update the comboBox entriesCombo with vessel ids
        '''
        self.comboBoxVessels.get_model().clear()
        self.comboBoxVessels.append_text("choose vessel")
        for vesselName in self.networkInfo[self.current_method][0]:
            self.comboBoxVessels.append_text(vesselName)
        self.comboBoxVessels.set_active(0)
                        
    def updateNetworkComboBox(self, networkCases, networkInfo):
        '''
        update the comboBox entriesCombo if availabel network names 
        have changed
        '''
        self.uqsaMethods = networkCases
        self.networkInfo = networkInfo
        
        self.comboBoxNetworks.get_model().clear()
        for casName in networkCases:
            self.comboBoxNetworks.append_text(casName)     
        
        if len(networkCases) > 1:
            self.comboBoxNetworks.set_active(1)
            self.current_method = self.uqsaMethods[1]
        else:
            self.comboBoxNetworks.set_active(0)
            self.current_method = self.uqsaMethods[0]
        
    def on_changedCBvessels(self, widget):
        '''
        call function of the combobox to choose vessel of the network
        '''
        cbIndex = widget.get_active()
        if cbIndex <= 0 : self.currentVesselId = None
        else: self.currentVesselId = self.networkInfo[self.current_method][0][cbIndex - 1]
        
    def on_changedNetworkComboBox(self, widget):
        '''
        call function of the combobox to choose solution data set number
        '''
        cbIndex = widget.get_active()
        #TODO: Try Except Pass should be fixed
        try: self.current_method = self.uqsaMethods[cbIndex]
        except: pass
        self.updateVesselComboBox()
        self.networkDescription.set_text(self.networkInfo[self.current_method][1])

class Visualisation2DMainGUI(gtk.Window):
    '''
    Class defining the GUI of the visualisation main window
    '''
    def __init__(self):
        '''
        Initialize
        '''
        self.networkInfo = {"choose simulation case" : [[], '-'] }
        self.networkCases = ["choose simulation case"]
        
        super(Visualisation2DMainGUI, self).__init__()
        # variables for windows                
        self.set_size_request(650, 290)
        self.set_position(gtk.WindowPosition.CENTER)
        self.connect("destroy", gtk.main_quit)
        self.mainTitle = "2D Visualisation - "
        self.set_title(self.mainTitle)
        self.set_resizable(False)
                       
        # open Button
        self.buttonOpenSolutionData = gtk.Button("Open solutionData")
        self.buttonOpenSolutionData.connect("clicked", self.on_clickedLoad)
        self.buttonOpenSolutionData.set_size_request(200, 35)
        
        # open plot window
        self.buttonOpenPlotWindow = gtk.Button("Show plots")
        self.buttonOpenPlotWindow.connect("clicked", self.on_clickedPlots)
        self.buttonOpenPlotWindow.set_size_request(120, 35)
        
         # add new Case
        self.cases = []
        self.buttonAddCase = gtk.Button("Add case")
        self.buttonAddCase.connect("clicked", self.on_clickedAddCase)
        self.buttonAddCase.set_size_request(120, 35)
        
        # ExternalData
        self.extDataDescription = gtk.Label("-")
        self.extDataDescription.set_size_request(400, 35)
        # open Button
        self.buttonOpenExternalData = gtk.Button('Open external data for comparison')
        self.buttonOpenExternalData.connect("clicked", self.on_clickedLoadExternal)
        self.buttonOpenExternalData.set_size_request(250, 35)
        # enable check box
        self.buttonEnableExternalData = gtk.CheckButton("plot external data")
        self.buttonEnableExternalData.set_size_request(150, 35)
        self.buttonEnableExternalData.set_active(0)
        
        # alignment of the boxes
        self.vBox = gtk.VBox(False, 10)
        
        # Load And Plot buttons
        hBox1 = gtk.HBox(False, 10)
        
        spacingText25Box1 = gtk.Label('')
        spacingText25Box1.set_size_request(25, 35)
        hBox1.pack_start(spacingText25Box1, fill=False, expand=False, padding=0)
        hBox1.pack_start(self.buttonOpenSolutionData, fill=False, expand=False, padding=0)
        hBox1.pack_start(self.buttonOpenPlotWindow, fill=False, expand=False, padding=0)
        separator1 = gtk.HSeparator()
        
        self.vBox.pack_start(hBox1, expand=False, fill=False, padding=0)
        self.vBox.pack_start(separator1, expand=False, fill=False, padding=0)
        
        # # add first case button
        newCase = Visualisation2DMainCase(len(self.cases) + 1)
        self.cases.append(newCase)
        self.vBox.pack_start(newCase.vBoxCase, expand=False, fill=False, padding=0)
                
        # add more button
        hBoxAdd = gtk.HBox(False, 10)
        spacingText25Add = gtk.Label('')
        spacingText25Add.set_size_request(25, 35)
        hBoxAdd.pack_start(spacingText25Add, fill=False, expand=False, padding=0)
        hBoxAdd.pack_start(self.buttonAddCase, fill=False, expand=False, padding=0)
        separator2 = gtk.HSeparator()
        
        self.vBox.pack_start(hBoxAdd, expand=False, fill=False, padding=0)
        self.vBox.pack_start(separator2, expand=False, fill=False, padding=0)
                                
        # external Data Set
        
        spacingText25Box4text = gtk.Label('')
        spacingText25Box4text.set_size_request(15, 25)
        spacingText25Box4Desc = gtk.Label('')
        spacingText25Box4Desc.set_size_request(25, 35)
        
        hBoxExternalData = gtk.HBox(False, 10)
        hBoxExternalDecription = gtk.HBox(False, 10)
        hBoxExternalData.pack_start(spacingText25Box4text, fill=False, expand=False, padding=0)
        hBoxExternalData.pack_start(self.buttonOpenExternalData, fill=False, expand=False, padding=0)
        hBoxExternalData.pack_start(self.buttonEnableExternalData, fill=False, expand=False, padding=0)
        hBoxExternalDecription.pack_start(spacingText25Box4Desc, fill=False, expand=False, padding=0)
        hBoxExternalDecription.pack_start(self.extDataDescription, fill=False, expand=False, padding=0)
        
        # external Data Set
        self.vBox.pack_start(hBoxExternalData, expand=False, fill=False, padding=0)
        self.vBox.pack_start(hBoxExternalDecription, expand=False, fill=False, padding=0)
                     
        self.add(self.vBox)
        self.show_all()
        
    def on_clickedAddCase(self, widget):
        '''
        add new simulation case to compare plots
        bounded to 5 cases now ..
        '''
        if len(self.cases) < 6:
            newCase = Visualisation2DMainCase(len(self.cases) + 1)
            newCase.updateNetworkComboBox(self.networkCases, self.networkInfo)
            self.cases.append(newCase)
            self.vBox.pack_start(newCase.vBoxCase, expand=False, fill=False, padding=0)
            self.vBox.reorder_child(newCase.vBoxCase, len(self.cases) + 1)
            
            width, height = self.get_size()
            self.set_size_request(width, height + 102)
            
            self.show_all()
                
    def on_clickedLoad(self, widget):
        pass
    
    def on_clickedPlots(self, widget):
        pass
        
    def on_clickedLoadExternal(self):
        pass
    
class Visualisation2DMain(Visualisation2DMainGUI):
    '''
    Class for the Main GUI window
    '''
    def __init__(self, networkName=None, dataNumber=None, connect=None):
        super(Visualisation2DMain, self).__init__()

        self.externalData = None
        
        self.networks     = { "choose simulation case"   : None}
        self.networkCases = [ "choose simulation case"]
        self.networkInfo  = { "choose simulation case"   : [[], '-'] }
        self.uqsaCase = None
        self.baseNetwork = None
        self.loadVascularNetwork(networkName, dataNumber)

    def on_clickedPlots(self, widget):
        '''
        Open new plot window to plot information of all selected vessels of the selected cases  
        '''
        for case_idx, case in enumerate(self.cases):
            if case.current_method == "choose simulation case" or case.currentVesselId is None:
                return
        Visualisation2DPlotWindow(self.uqsaCase, self.cases)


    def loadVascularNetwork(self, networkName, dataNumber, networkXmlFile = None, pathSolutionDataFilename = None):
        '''
        This function actually loads the vascular networks with the given names and ids
            networkName
        The network is added to the existing ones if is not existing
        '''
        #  TODO load multiple?
        self.networkCases = [ "choose simulation case"]
        uqsaCase = cUqsaCase.UqsaCase()
        uqsaCaseFile = mFPH_VPC.getFilePath('uqsaCaseXmlFile', networkName, dataNumber, 'read')
        uqsaCase.loadXMLFile(uqsaCaseFile)
        uqsaCase.initialize(networkName, dataNumber)
        # 1.2 load vascular network file that will implement polynomial chaos
        vpcNetworkXmlFile = mFPH_VPC.getFilePath('vpcNetworkXmlFile', networkName, dataNumber, 'read')
        vascularNetwork = mXML.loadNetworkFromXML(networkName, dataNumber, networkXmlFile=vpcNetworkXmlFile)
        # TODO ensure this is reloadable
        vascularNetwork.randomInputManager.initialize(vascularNetwork)
        assert len(
            vascularNetwork.randomInputManager.randomInputs.keys()) != 0, "VascularPolynomialChaos_v0.3: no random inputs defined!"
        vascularNetwork.randomInputManager.printOutInfo()
        distributionManager = cDistMng.DistributionManagerChaospy(
            vascularNetwork.randomInputManager.randomInputsExtDist)
        distributionManager.createRandomVariables()
        uqsaCase.aquireSamples(distributionManager, vascularNetwork.randomInputManager.randomInputsExtDist)

        # Get path to solution data
        caseName = '_'.join([uqsaCase.sampleManager.samplingMethod])

        uqsaMethods = [method for method in uqsaCase.uqsaMethods]
        locations = [loc.queryLocation for loc in uqsaCase.locationOfInterestManager.locationsOfInterest.values()]
        locations = []

        for method in uqsaCase.uqsaMethods:
            self.networkCases.append(method)
            self.networkInfo[method] = [locations, '#TODO']

            # Get path to solution data
        caseName = '_'.join([uqsaCase.sampleManager.samplingMethod])
        uqsaSolutionDataFile = mFPH_VPC.getFilePath('uqsaSolutionDataFile', networkName, dataNumber,
                                                    mode="read", caseName=caseName)
        uqsaCase.locationOfInterestManager.openQuantityOfInterestFile(uqsaSolutionDataFile, mode='r')

        for name, location in uqsaCase.locationOfInterestManager.locationsOfInterest.items():
            locations.append(location.queryLocation)
            for qoi_name, qoi in location.quantitiesOfInterest.items():
                for uqsaMethodName in uqsaCase.uqsaMethods:
                    uqsaMeasures = UqsaMeasures()
                    qoi.addUqsaMeasures(uqsaMethodName, uqsaMeasures)

        uqsaCase.locationOfInterestManager.updateHdf5Groups('LocationOfInterestManager')
        uqsaCase.locationOfInterestManager.loadDataHdf5()

        self.uqsaCase = uqsaCase
        self.baseNetwork = vascularNetwork

        # update comboBoxes
        for case in self.cases:
            case.updateNetworkComboBox(self.networkCases, self.networkInfo)

    def on_clickedLoad(self, widget):
        '''
        Call function of the Open Solution Data Button
        '''
        fileFilter = gtk.FileFilter()
        fileFilter.set_name("SolutionData")
        fileFilter.add_pattern("*.hdf5")
        
        filenames = self.LoadDialog(fileFilter)
        
        for filename in filenames:
            if '_SolutionData_' in filename: # normal network cases
                basename = os.path.split(filename)[-1]
                networkName = basename.split('_SolutionData_')[0]
                dataNumber = basename.split('_SolutionData_')[1].split('.')[0]
                #networkName = filename.split('/')[-1].split('_SolutionData_')[0]
                #dataNumber = filename.split('/')[-1].split('_SolutionData_')[1].split('.')[0]
                if '_'.join([networkName, dataNumber]) not in self.networkCases:
                    self.loadVascularNetwork(networkName, dataNumber)
            elif '_evaluation_' in filename:# polynomial chaos cases
                # TODO cross-platform fixes
                networkFileName = ''.join([filename.split('/')[-1].split('.')[0],'.xml'])
                networkName = filename.split('/')[-1].split('_evaluation_')[0]
                evaluationNumber = filename.split('/')[-1].split('_evaluation_')[1].split('.')[0]
                networkXmlFile = ''.join(['/'.join(filename.split('/')[0:-2]),'/evaluationNetworkFiles/',networkFileName])
                if '_'.join([networkName, evaluationNumber]) not in self.networkCases:
                    self.loadVascularNetwork(networkName, evaluationNumber,networkXmlFile,filename)
                
                          
    def LoadDialog(self, fileFilter):
        '''
        Dialog window function: 
        displaying the file browser where solution data files can be selected
        to be loaded into the program.
        
        return: [filename,..] (abs-path to the files)
        '''
        dialog = gtk.FileChooserDialog("Open Solution Data File..",
                                     None,
                                     gtk.FileChooserAction.OPEN,
                                     (gtk.STOCK_CANCEL, gtk.ResponseType.CANCEL,
                                      gtk.STOCK_OPEN, gtk.ResponseType.OK))
        dialog.set_default_response(gtk.ResponseType.OK)
        
        directory = mFPH.getDirectory('workingDirectory', '', '', mode= 'read')
        dialog.set_current_folder(directory)
        
        dialog.set_select_multiple(True)
        
        dialog.add_filter(fileFilter)
        
        filenames = []
        
        response = dialog.run()
        
        if response == gtk.ResponseType.OK:
            filenames = dialog.get_filenames()
        elif response == gtk.ResponseType.CANCEL:
            print('Closed, no files selected')
        dialog.destroy()    
        return filenames
    

def main():
    optionsDict = moduleStartUp.parseOptions(['f', 'n'], vascularPolynomialChaos=True)
    
    networkName = optionsDict['networkName']
    dataNumber = optionsDict['dataNumber']

    Visualisation2DMain(networkName=networkName, dataNumber=dataNumber)
    gtk.main()

if __name__ == '__main__':
    main()
               
    
