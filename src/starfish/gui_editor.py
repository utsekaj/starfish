import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
import starfish.UtilityLib.moduleStartUp as moduleStartUp
import starfish.UtilityLib.moduleXML as mXML
from starfish.UtilityLib.constants import variablesDict
from starfish.UtilityLib.constants import newestNetworkXml as nxmlW


class MainWindow(gtk.Window):
    def __init__(self, networkName, dataNumber):
        super(MainWindow, self).__init__()
        self.network = None
        self.active_vessel = None
        self.currentVesselName = None
        self.currentVesselId = None
        self.comboBoxVessels = gtk.ComboBoxText()
        self.vesselNames = []
        self.loadVascularNetwork(networkName, dataNumber)
        self.currentVesselName = self.vesselNames[0]
        self.currentVesselId = int(self.currentVesselName.split('_')[-1])
        self.comboBoxVessels.connect("changed", self.on_changedCBvessels)
        self.comboBoxVessels.set_active(0)
        self.vBox = gtk.VBox(False, 10)
        hBoxButtons = gtk.HBox(False, 10)

        self.width = 130 + 120
        self.height = 30 + 30
        self.countHeight = 1
        hBoxButtons.set_size_request(self.width, self.height)
        hBoxButtons.pack_start(self.comboBoxVessels, fill=False, expand=False, padding=0)
        self.vBox.pack_start(hBoxButtons, expand=False, fill=False, padding=0)
        self.set_size_request(self.width, self.height)

        self.entries = []
        self.inputVbox = gtk.VBox(False, 1)
        self.initializeInput()
        self.vBox.pack_start(self.inputVbox, expand=True, fill=True, padding=0)
        self.add(self.vBox)
        self.connect("destroy", gtk.main_quit)
        self.show_all()

    def loadVascularNetwork(self, networkName, dataNumber, networkXmlFile=None):
        '''
        '''
        # load vascular network
        vascularNetwork = mXML.loadNetworkFromXML(networkName, dataNumber=dataNumber, networkXmlFile=networkXmlFile)
        # # save it and refresh GUi setup
        networkSolutionName = '_'.join([networkName, 'new'])
        # # add data name and corresponding network
        self.network = vascularNetwork
        # get vessel names
        vesselNames = []
        for key, vessel in vascularNetwork.vessels.items():
            vesselNames.append('{:3}-{}'.format(vessel.Id, vessel.name))
        # # reference network showing network case name and [corresponding number of vessels, vesselNames, description]
        self.vesselNames = vesselNames
        self.updateVesselComboBox()

    def initializeInput(self):
        self.countHeight = 1
        for entry in self.entries:
            self.inputVbox.remove(entry[-1])
        # for child in self.inputVbox.get_children():
        # self.inputVbox.remove(child)
        self.entries = []
        currentVesselId = self.currentVesselId
        vessel = self.network.vessels[currentVesselId]
        xmlElementName = 'vessels'
        xmlElement = nxmlW.xmlElementsReference[xmlElementName]

        for attribute in nxmlW.vesselAttributes:
            attributeValue = str(vessel.getVariableValue(attribute))
            self.createTextInput(self.inputVbox, attribute, attributeValue)

        for idx, vesselElement in enumerate(xmlElement):
            callback = None
            if vesselElement == 'compliance':
                complianceType = vessel.getVariableValue('complianceType')
                variables = nxmlW.vesselElementReference[vesselElement][complianceType]
                callback = self.on_changedComplianceType
            else:
                variables = nxmlW.vesselElementReference[vesselElement]
            for variable in variables:
                value = vessel.getVariableValue(variable)
                self.createSingleInput(self.inputVbox, variable, value)
                if callback is not None:
                    self.entries[-1][1].connect('changed', callback)

        self.set_size_request(self.width, 30 * (self.countHeight + 1))

    def updateNetwork(self):
        vessel = self.network.vessels[self.currentVesselId]
        new_data = dict()
        for entry in self.entries:
            label, box = entry[0:2]
            variable = label.get_text()
            if type(box) is gtk.ComboBoxText:
                value = box.get_active_text()
            else:
                value = box.get_text()
            new_data[variable] = mXML.loadVariablesConversion(variable, value, None)
        vessel.setVariablesDict(new_data)

    def createSingleInput(self, vbox, variable, value):
        variableTypes = variablesDict[variable]['type']
        comboBoxValues = variablesDict[variable]['strCases']

        if ' ' in variableTypes:
            variableTypes = variableTypes.split(' ')
        else:
            variableTypes = [variableTypes]
        variableTypes.sort()

        if len(variableTypes) == 1 and 'bool' in variableTypes:
            comboBoxValues = ('True', 'False')

        if comboBoxValues is not None and 'anything' not in comboBoxValues:
            self.createComboBoxInput(vbox, variable, comboBoxValues, value)
        else:
            self.createTextInput(vbox, variable, value)

    def createTextInput(self, vbox, variable, value):
        hbox = gtk.HBox(False, 10)
        label = gtk.Label(variable)
        hbox.pack_start(label, fill=False, expand=True, padding=0)
        self.countHeight += 1
        entry = gtk.Entry()
        entry.set_size_request(120, 20)
        entry.set_text(str(value))  # TODO
        hbox.pack_start(entry, fill=False, expand=False, padding=0)
        vbox.pack_start(hbox, expand=False, fill=False, padding=0)
        self.entries.append((label, entry, hbox))

    def createComboBoxInput(self, vbox, variable, comboChoices, value):
        hbox = gtk.HBox(False, 10)
        label = gtk.Label(variable)
        hbox.pack_start(label, fill=False, expand=False, padding=0)
        self.countHeight += 1
        entry = gtk.ComboBoxText()
        for choice in comboChoices:
            entry.append_text(choice)
        entry.set_active(comboChoices.index(str(value)))  # comboChoices.index(value))
        entry.set_size_request(120, 30)
        hbox.pack_start(entry, fill=False, expand=False, padding=0)
        vbox.pack_start(hbox, expand=False, fill=False, padding=0)
        self.entries.append((label, entry, hbox))

    def on_changedComplianceType(self, widget):
        cbtext = widget.get_active_text()
        self.network.vessels[self.currentVesselId].complianceType = cbtext
        self.initializeInput()
        self.show_all()

    def on_changedCBvessels(self, widget):
        self.updateNetwork() #TODO handle changing vessel names
        cbtext = widget.get_active_text()
        self.currentVesselName =  widget.get_active_text() #self.vesselNames[cbIndex]
        self.currentVesselId = int(self.currentVesselName.split('_')[-1])
        self.initializeInput()
        self.show_all()

    def updateVesselComboBox(self):
        '''
        Update the comboBox entriesCombo with vessel ids
        '''
        self.comboBoxVessels.get_model().clear()
        for vesselName in self.vesselNames:
            self.comboBoxVessels.append_text(vesselName)
        self.comboBoxVessels.set_active(0)


def main():
    optionsDict = moduleStartUp.parseOptions(['f', 'n'], visualisationOnly=True)
    networkName = optionsDict['networkName']
    dataNumber = optionsDict['dataNumber']
    MainWindow(networkName, dataNumber)
    gtk.main()


if __name__ == "__main__":
    main()
