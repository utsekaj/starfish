##!/usr/bin/env python
# -*- coding: utf-8 -*- ####################################################################################### STARFiSh v0.4 
########################################################################################
## 
# http://www.ntnu.no/starfish
#
# Contributors:
# Leif Rune Hellevik, Vinzenz Gregor Eck, Jacob Sturdy, Fredrik Eikeland Fossan, 
# Einar Nyberg Karlsen, Yvan Gugler, Yapi Donatien Achou, Hallvard Moian Nydal, 
# Knut Petter Maråk, Paul Roger Leinan
#
# TODO: ADD LICENSE (MIT) and COPYRIGHT
#
#Copyright (c) <2012-> <NTNU>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this 
#software and associated documentation files (the "Software"), to deal in the Software
#without restriction, including without limitation the rights to use, copy, modify,
#merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
#permit persons to whom the Software is furnished to do so, subject to the following 
# conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
#PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
#CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.
##

#---------------------------------------------------------------------------------------#
import time 
import sys,os
import subprocess
import logging
# set the path relative to THIS file not the executing file!
logger = logging.getLogger(__name__)

import starfish.SolverLib.class1DflowSolver as c1DFlowSolv
import starfish.UtilityLib.moduleXML as mXML
import starfish.UtilityLib.moduleStartUp as mStartUp
import starfish.UtilityLib.moduleFilePathHandler as mFPH


def main(networkName, dataNumber, simulationDescription, resimulate, params=None):
    # optionsDict = mStartUp.parseOptions(['f','n','d','r'])
    #
    # networkName           = optionsDict['networkName']
    # dataNumber            = optionsDict['dataNumber']
    # simulationDescription = optionsDict['simulationDescription']
    # resimulate            = optionsDict['resimulate']
    
    filename = str(networkName+'.xml')
        
    logger.info('____________Simulation_______________')
    logger.info('%-20s %s' % ('Network name',networkName))
    logger.info('%-20s %s' % ('Data number', dataNumber))
    logger.info('%-20s %s' % ('Case description', simulationDescription))
    logger.info('%-20s %s' % ('Resimulate', resimulate))
    
    ## check if template
    if '_template' in networkName:
        networkName = mFPH.createWorkingCopyOfTemplateNetwork(networkName)
    
    # load network from the path!
    if resimulate == False:
        vascularNetwork = mXML.loadNetworkFromXML(networkName)
    else:
        # resimulate network
        vascularNetwork = mXML.loadNetworkFromXML(networkName, dataNumber=dataNumber)
        if simulationDescription == '':
            simulationDescription = vascularNetwork.description

    if vascularNetwork == None: exit()

    command_line_params = {}
    if params is not None:
        for param in params:
            name, value = param.split('=')
            if name in mXML.variablesDict:
                value = mXML.loadVariablesConversion(name, value, None)
                command_line_params[name] = value
            else:
                logger.error(f'Arugment {param} passed as --params but not defined in mXML.variablesDict')
                logger.error(f'Valid --params are {mXML.variablesDict.keys()}')
                exit()
    vascularNetwork.update(command_line_params)
    
    vascularNetwork.update({'description':simulationDescription,
                            'dataNumber' :dataNumber})
    
    timeSolverInitStart = time.time()
    #initialize Solver
    flowSolver = c1DFlowSolv.FlowSolver(vascularNetwork)
    timeSolverInit = time.time()-timeSolverInitStart
    timeSolverSolveStart = time.time()
    #solve the system
    flowSolver.solve()
    timeSolverSolve = time.time()-timeSolverSolveStart
    
    minutesInit = int(timeSolverInit/60.)
    secsInit = timeSolverInit-minutesInit*60.
    minutes_solve = int(timeSolverSolve/60.)
    secsSolve = timeSolverSolve-minutes_solve*60.


    logger.info('____________ Solver time _____________')
    logger.info('Initialisation: {} min {} sec'.format(minutesInit, secsInit))
    logger.info('Solving:        {} min {} sec'.format(minutes_solve, secsSolve))
    logger.info('=====================================')
    print('Initialisation: {} min {} sec'.format(minutesInit, secsInit))
    print('Solving:        {} min {} sec'.format(minutes_solve, secsSolve))
    vascularNetwork.saveSolutionData()
    mXML.writeNetworkToXML(vascularNetwork, dataNumber = dataNumber) # needs to be moved to vascularNetwork
    mFPH.updateSimulationDescriptions(networkName, dataNumber, simulationDescription)

#if __name__ == '__main__':
#    main()
