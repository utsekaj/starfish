#!/usr/bin/env python
# -*- coding: utf-8 -*- 
########################################################################################
#                           STARFiSh v0.4 
########################################################################################
# #
# http://www.ntnu.no/starfish
#
# Contributors:
# Leif Rune Hellevik, Vinzenz Gregor Eck, Jacob Sturdy, Fredrik Eikeland Fossan, 
# Einar Nyberg Karlsen, Yvan Gugler, Yapi Donatien Achou, Hallvard Moian Nydal, 
# Knut Petter Maråk, Paul Roger Leinan
#
#
#
# Copyright (c) <2012-> <NTNU>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this 
#software and associated documentation files (the "Software"), to deal in the Software 
#without restriction, including without limitation the rights to use, copy, modify, 
#merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
#permit persons to whom the Software is furnished to do so, subject to the following 
# conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
#PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
#CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#---------------------------------------------------------------------------------------#
import os
import sys
import pathlib
import subprocess
import logging

import starfish.UtilityLib.networkXml043
from starfish import gui_editor

logger = logging.getLogger(__name__)
file_formatter = logging.Formatter('%(name)s - %(lineno)d -  %(levelname)s - %(message)s')

std_formatter = logging.Formatter('%(message)s')
logger = logging.getLogger('starfish')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("starfish.log", mode="w") #TODO better formatting
fh.setLevel(logging.DEBUG)
fh.setFormatter(file_formatter)
logger.addHandler(fh)
std= logging.StreamHandler()
std.setLevel(logging.INFO)
std.setFormatter(std_formatter)
logger.addHandler(std)

import argparse
import time
import starfish.Simulator as simulator
import starfish.VascularPolynomialChaos as uqsaToolBox

import starfish.SolverLib.class1DflowSolver as c1DFlowSolv
import starfish.UtilityLib.moduleXML as mXML
import starfish.UtilityLib.moduleStartUp as mStartUp
import starfish.UtilityLib.moduleFilePathHandler as mFPH

if 'win' in sys.platform:
    DISPLAY = True # assume display in windows
else:
    DISPLAY = 'DISPLAY' in os.environ

if DISPLAY:
    try:
        import starfish.Visualisation as visualisationToolBox
        NOVIZ=False
    except ImportError: 
        NOVIZ=True
    try:
        import starfish.vnc as vnc
        NOVNC=False
    except ImportError or RuntimeError:
        NOVNC=True
else:
    logger.debug('DISPLAY not found in environment.')
    NOVIZ=True
    NOVNC=True

# TODO:
# https://github.com/kislyuk/argcomplete
# https://gist.github.com/mivade/384c2c41c3a29c637cb6c603d4197f9f

cli = argparse.ArgumentParser()
subparsers = cli.add_subparsers(dest="subcommand")
def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.

    https://gist.github.com/mivade/384c2c41c3a29c637cb6c603d4197f9f
    """
    return (list(name_or_flags), kwargs)


def subcommand(args=[], parent=subparsers, parents=[]):
    """Decorator to define a new subcommand in a sanity-preserving way.
    The function will be stored in the ``func`` variable when the parser
    parses arguments so that it can be called directly like so::
        args = cli.parse_args()
        args.func(args)
    Usage example::
        @subcommand([argument("-d", help="Enable debug mode", action="store_true")])
        def subcommand(args):
            print(args)
    Then on the command line::
        $ python cli.py subcommand -d

    https://gist.github.com/mivade/384c2c41c3a29c637cb6c603d4197f9f
    """
    def decorator(func):
        parser = parent.add_parser(func.__name__, description=func.__doc__, parents=parents)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator


description = argument('-d', '--description', dest='description',
                    help="simulation case description; NB: must be quoted if spaces are present")
reduced_file = argument("-e", "--reducedFile", dest='new_network_name',
                    help="name of reduced network name")
network_name = argument("-f", "--file", dest='network_name',
                    help="open file with given network name")
data_number = argument("-n", "--data-number", dest='data_number',
                    help="number of the solution data (last number in filename)",)
data_type = argument("-t", "--data-type", dest='data_type',
                     help='what type of data to load',
                     choices=('simulation', 'convergence', 'uqsa'),
                     default='simulation')
resimulate = argument("-r", "--resimulate", action="store_true", dest="resimulate",
                    help="resimulate case with same network saved in datanumber file, 0 = False, 1 = True")
vis_mode = argument('-v', '--visualization', dest='visualization', choices=['2D+3D', '2D', '3D', ],
                    help="choose visualisation mode, 1: 2d and 3d, 2: 2d plots, 3: 3d visualisation",
                    default='2D')

valid_params = (starfish.UtilityLib.networkXml043.vascularNetworkElements +
                starfish.UtilityLib.networkXml043.simulationContextElements +
                starfish.UtilityLib.networkXml043.solverCalibrationElements)
params = argument('-p', '--params', dest='params',
                  help=f"specify parameter values for simulation as 'par1=value1 par2=value2 ...' with values given in kg-m-s. Valid params are {valid_params}",
                  nargs='*')


def main():
    args = cli.parse_args()
    if args.subcommand is None:
        old_main(args)
    else:
        args.func(args)

@subcommand([network_name, data_number, description, resimulate, params], parent=subparsers)
def simulate(args):
    networkName = args.network_name
    dataNumber = args.data_number
    simulationDescription = args.description
    resimulate = args.resimulate
    if networkName is None:
        networkName = mStartUp.chooseNetwork()
    if simulationDescription is None:
        simulationDescription =  mStartUp.defineSimulationDescription()
    if dataNumber is None:
        dataNumber =  mStartUp.defineDataNumber(networkName)
    simulator.main(networkName, dataNumber, simulationDescription, resimulate, args.params)

@subcommand([network_name, data_number, data_type, vis_mode], parent=subparsers)
def visualize(args):
    networkName = args.network_name
    dataNumber = args.data_number
    dataType = args.data_type
    if networkName is None:
        networkName, dataNumber, dataType = mStartUp.chooseSolutionDataCase()
    elif dataNumber is None:
        networkName, dataNumber, dataType = mStartUp.chooseSolutionDataCase(networkName=networkName)
    visualisationToolBox.main(networkName, dataNumber, dataType, args.visualization)

@subcommand([network_name,
             data_number,
             argument('-c', '--uqsa-steps', dest='uqsa_steps',
                      choices=['create', 'run', 'create+run'],
                      default='create')
             ], parent=subparsers)
def uqsa(args):
    networkName = args.network_name
    dataNumber = args.data_number
    uqsaCommand = args.uqsa_steps
    if networkName is None:
        networkName = mStartUp.chooseNetwork()
    if dataNumber is None or uqsaCommand is None:
        dataNumber, uqsaCommand = mStartUp.chooseUQSACaseFile(networkName)
    uqsaToolBox.uncertaintyPropagation(networkName, dataNumber, uqsaCommand)

def tmp_main():
    valid_commands = {'simulate': simulator.main,
                      'visualize': visualisationToolBox.main,
                      'uqsa': uqsaToolBox.uncertaintyPropagation,
                      'edit': gui_editor.main}

    parser = argparse.ArgumentParser(
        description='Run the STARFISH user interface to perform simulations or visualize results',
        usage='''starfish <command> [<args>]

    The most commonly used starfish commands are:
       simulate   run the solver for  single simulation
       visualize  run the visualization app to view simulation results
       uqsa       run the UQSA toolbox to generate and run a UQSA case
       edit       run the interactive network editor
    ''')
    parser.add_argument('command', help=f'Subcommand to run: {", ".join(valid_commands.keys())}')

    args = parser.parse_args(sys.argv[1:2])
    if not ('command' in args) or not (args.command in valid_commands):
        old_main()
    else:
        valid_commands[args.command]()

def old_main(args=None):
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print("!!!! Warning this menu is deprecated. See starfish --help for suggested usage !!!!")
    print()
    print('=====================================')
    print('#     STARFiSh_v0.5.06.10.2021      #')
    print('=====================================')
    
    mainMenuInput = ""
    while mainMenuInput not in ['q']:
        print('\n Main menu:\n')
        print(" [1] - run simulation | Main.py")
        if NOVNC:
            VNCSTR="(Not Available)"
        else:
            VNCSTR=""
        print(" [2] -{} run vnc (vascular network creator) | vnc.py".format(VNCSTR))
        if NOVIZ:
            VIZSTR="(Not Available)"
        else:
            VIZSTR=""
        print(" [3] -{} run visualisation | Visualisation.py".format(VIZSTR))
        print(" [4] - run uncertainty quantification tool box | VascularPolynomialChaos.py")
        print(" [5] - run graphical network editor | gui_editor.py")
        print(" [q] - quit \n")
        while  mainMenuInput not in ('1','2','3','4', '5', 'q'):
            mainMenuInput =input("what to do? ")
        
        if mainMenuInput == '1':
            print("\n .. running simulation \n")
            networkName = mStartUp.chooseNetwork()
            simulationDescription = mStartUp.defineSimulationDescription()
            dataNumber = mStartUp.defineDataNumber(networkName)
            simulator.main(networkName, dataNumber, simulationDescription, False)
            mainMenuInput = ""
            
        if mainMenuInput == '2' and NOVNC:
            print("\n .. vnc not available \n")
            mainMenuInput = ""
        elif mainMenuInput == '2':
            print("\n .. running vnc \n")
            exec_string = ' '.join([sys.executable, '-c "import starfish.vnc as vnc; vnc.main()"'])
            vnc_proc = subprocess.Popen(exec_string, shell=True)
            while vnc_proc.poll() is None:
                pass
            vnc_proc.terminate()
            mainMenuInput = ""
            
        if mainMenuInput == '3' and NOVIZ:
            print("\n .. visualisation not available \n")
            mainMenuInput = ""
        elif mainMenuInput == '3':    
            print("\n .. running visualisation \n")
            networkName, dataNumber, dataType = mStartUp.chooseSolutionDataCase()
            visualisationToolBox.main(networkName, dataNumber, dataType, '2D')
            mainMenuInput = ""

        if mainMenuInput == '4':
            print("\n .. running uncertainty quantification tool box \n")
            networkName = mStartUp.chooseNetwork()
            dataNumber, uqsaCommand = mStartUp.chooseUQSACaseFile(networkName)
            uqsaToolBox.uncertaintyPropagation(networkName, dataNumber, uqsaCommand)
            mainMenuInput = ""

        if mainMenuInput == '5':
            print("\n .. running visualisation \n")
            gui_editor.main()
            mainMenuInput = ""

if __name__ == '__main__':
    main()
