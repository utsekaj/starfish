#!/usr/bin/env python
from __future__ import print_function, absolute_import
from future.utils import iteritems, iterkeys, viewkeys, viewitems, itervalues, viewvalues
from builtins import input as input3
import subprocess

from optparse import OptionParser
import os,sys
cur = os.path.dirname( os.path.realpath( __file__ ) )

#sys.path.append(cur+'/UtilityLib')
import starfish
import starfish.UtilityLib.moduleStartUp as mStartUp

def main(networkName, dataNumber, dataType, vizOutput):
    print("")
    print('=====================================')
    print('#    STARFiSh_v0.5 Visualisation    #')
    print('=====================================')
    print("")
    #
    # optionsDict = mStartUp.parseOptions(['f','n','v'], visualisationOnly = True)
    # networkName    = optionsDict['networkName']
    # dataNumber     = optionsDict['dataNumber']
    # dataType = optionsDict['dataType']
    # vizOutput      = optionsDict['vizOutput']
    if vizOutput == 'non':
        vizOutput = "2D+3D"

    viz_strings = {}
        # 'Solution':'"import starfish.VisualisationLib.class2dVisualisation as viz;"',
        # 'Convergence':'"import starfish.VisualisationLib.class2dConvergenceVisualisation as viz;"',
        # 'uqsa':'"import starfish.VisualisationLib.class2dUqsaVisualisation as viz;"',}

    string2d = ' '.join([sys.executable, '-c'
                        '"import starfish.VisualisationLib.class2dVisualisation as viz;',
                         "viz.main()", '"',
                         '-f', networkName,
                         '-n', str(dataNumber)])
    viz_strings['simulation'] = string2d

    string2d = ' '.join([sys.executable, '-c'
                        '"import starfish.VisualisationLib.class2dUqsaVisualisation as viz;',
                         "viz.main()", '"',
                         '-f', networkName,
                         '-n', str(dataNumber)])
    viz_strings['uqsa'] = string2d

    string2d = ' '.join([sys.executable, '-c'
                        '"import starfish.VisualisationLib.class2dConvergenceVisualisation as viz;',
                         "viz.main()", '"',
                         '-f', networkName,
                         '-n', str(dataNumber)])
    viz_strings['convergence'] = string2d


    string3d = ' '.join([sys.executable, '-c' 
        '"import starfish.VisualisationLib.class3dVisualisation as viz;', 
        "viz.main()", '"', 
        '-f', networkName, 
        '-n',str(dataNumber)])
    
    if vizOutput == "2D":
        subprocess.Popen(viz_strings[dataType], shell=True)
        
    if vizOutput == "3D":
        viz3d = subprocess.Popen(string3d, shell = True )
        
    if vizOutput == "2D+3D":
        viz2d = subprocess.Popen(string2d, shell = True )
        viz3d = subprocess.Popen(string3d, shell = True )
        while viz2d.poll() is None and viz3d.poll() is None:
            pass
                
if __name__ == '__main__':
    main()
