# List of confirmed tasks
* Figure out where to split xml042->043->044
* Move to github/bitbucket/ (something with an issue tracker)
* implement tests for: single-vessel, bifurcation, trifurcation, anastamosis 
* Test heart models
* Test Anastomosis
* Test UQSA
* Get started guide covering directory/config file
* How to handle "absolute" paths in template files/input files
* Debug convergence case tex file issues
* Follow up with Fredrik about the networkreduction test
* Add confirmation for rerunning samples/simulations of a UQSA case if they are already existing (Vinz)
* Update and verify the list of dependencies on Fedora (Jacob)
* Verify that template networks are working (Jacob)
* Delete baroreceptor references
* Remove realtime visualization from published templates
* Identify which templates should be released
* Which features should be highlighted/demonstrated in the tutorials? Heart models, venous models, data processing etc?


# Unassigned tasks
* starfish Menu: uqsa create file has as exit() and visualisation has exit() need to catched by main menu to stop exiting everything
* Check for (einar) TODOs
* Note that the WD must be given as an absolute path (or enable relative to the user's home folder (Could provide a sensible default folder in Documents a la MATLAB). But need system variables to that as OS specific
* Check TODOs
* Delete central/minimum VenousPressure tags from xmlTemplates
* Test venous system
* Test BRX
* Test gravity
* extract rotation and gravity into auxillary functions that can be applied

# List of components to fully separate
* Baroreflex
* Venous system
* Heart model data class
* Gravity

# List of ideas
* parameters may depend on "external" variables
* Implement generic component framework
* rename vascularPolynomialChaos to vascularUQSA 
* update path manipulations to use `os.path` 
* Make sure logging can be "quiet"
* Replace printouts with logging 
* (VNC) always able to "cancel" operation in VNC
* (VNC) Inform about geometry of vessel/how to change this
* (VNC) Missing As in Hayashi default


# List of done tasks
* Add confirmation of overwriting any networks in the working directory (Even simulation cases)  (Vinz)
* ask questions if -d, -v is not defined!!!
* update Main.py to be a universal interface for simulation, network creation and visualisation (Vinz)
* Venous system
* Update and verify the list of dependencies on Ubuntu (Jacob)
* Fix optional initiation of venous system (Jacob)
* Get website privileges for Fredrik, Jacob and Leif (Assigned to:Vinz)
* Fix WD manager so that WD's are successfully created from command line (Vinz)
* Ensure that the config file is generated when first running the code (Vinz)
* Fix vnc to not printout options for CSV BCs and RVs (Assigned to:Vinz)
* VNC never should crash while loading networks and vessels from xml and csv (delimiter still problematic). (Vinz)
* VNC shall detect delimiter and either warn/autofix rather than crash. (Vinz)
* flow from file inflowfile is set to absolute path. If template files are copied, a absoulte path is set (Fredrik)