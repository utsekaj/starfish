This is STARFiSh a program for simulation of 1D blood vessels with physiologically motivated
boundary conditions while accounting for input uncertainty.

# Test status

<https://utsekaj.gitlab.io/starfish/test_report> 

<https://utsekaj.gitlab.io/starfish>

# Getting started
## Installation
Assumes you already have a functional installation of `conda`. Use of [`miniforge`](https://github.com/conda-forge/miniforge) is recommended, but Anaconda should also work.

1. Open a conda prompt in this directory
2. `conda env create -f starfish/environment.yml -n stf_env`
3. `conda activate stf_env`
4. `pip install https://gitlab.com/utsekaj/starfish`


## Basic usage
Now the program `starfish` should be installed in the environment `stf_env`, so in a terminal with this environment active you can run
```starfish --help```
This will bring up the usage help for the command line interface (CLI) for `starfish`

The old menu based interface is still functioning as `starfish`, but not guaranteed for future development.

## Running your first simulation
Starfish assumes that the directory it is run from is a `working_directory` which means it will write and overwrite data
within this directory, so it's best to make a new directory and navigate to it to run starfish.
```bash
mkdir my_first_starfish_directory
cd my_first_starfish_directory
starfish simulate
```
should show a list of template networks should be presented. These are network definitions which come 
preconfigured in starfish. Select `singleCarotid` to test what happens. You will be prompted to enter a description of 
the simulation, this is only for later reference to remember what the solution data corresponds to. Additionally, you 
will be asked for a 3 character `Data Number` which is used to define where the solution data will be saved. 
Now the simulation should run.

## Viewing the results
If successful run
```bash
starfish visualize
```
and select your solution file. This should launch a menu to plot the results of this solution. As only one vessel is 
present you will not have many options, but this can be used to explore solution data from many vessel networks.

## More on the directory structure
The program handles data relative to the current directory where `starfish` is run. First a folder with the network name,
`singleCarotid` will be created and within this folder additional folders `SolutionData_xyz` will be created for each 
solution data set. `xyz` is a placeholder for the data number you selected.

If the solution has succeeded, you should be able to select visualization from the menu. Select 2D visualization. Select 
the network and data number you just created and then you will see a menu to select how to plot the solution data.

## Modifying networks
If you would like to change the settings of the solution, you can go into the folder 'singleVessel' and edit the file 
'singleVessel.xml'.
Then when you run `starfish` and select the network `singleCarotid` (note this is not the `singleCarotid_template`
selected previously), you will be able to run a new simulation with the settings you have just modified.

## UQSA with starfish
To see an example UQSA with starfish repeat the above, but for `singleVesselUQSA_template`. Once you have run this simulation run
```
starfish uqsa
```
The menu will ask if you want to generate a UQSA case file and exit, select this option.
Now in `my_first_starfish_directory/singleVesselUQSA/` you will have a folder `vascularPolynomialChaos_XXX` where `XXX` 
is the data number you specified.

You can run this case as is by executing `starfish uqsa` again and selecting the datanumber you specified. Once it
completes run
```bash
starfish visualize
```
and select the UQSA case. This will open a viewer for the analysis. (*NOTE* this only supports PC (polynomial chaos) 
analyses for the moment)
If you want to run a higher order expansion or analyze different output variables edit the file 
`singleVesselUQSA_uqsaCase_XXX.xml` and rerun.
If you want to change the input parameters edit `singleVesselUQSA_vpc_XXX.xml` in the UQSA case directory.

## Next steps
Many of the options and ways of specifying simulations are not well documented and require understanding of the code. 
(Hopefully a more complete documentation will be included here soon). Some options are only influential in some cases 
and others may be in conflict with each other. Don't hesitate to contact us with any questions or issues.
*TODO*

## Advanced command line interface
Each subcommand has a basic help
```bash
starfish simulate --help
starfish visualize --help
starfish uqsa --help
```
If running the same case or several cases you can avoid the interactive menu by specifying like
```
starfish simulate -f singleCarotid -n 123 -d 'my excellent simulation'
starfish visualize -f singleCarotid -n 123
```


## Python interface
Much of the above can be accomplished in python directly, so depending on your goals you may be interested in the python
interface.

*TODO*

# Developers guide

## Development install
If you want to make changes to the code first clone this repository 
1. Open a conda prompt in this directory
2. `conda env create -f starfish/environment.yml -n stf_env`
3. `conda activate stf_env`
4. `git clone https://gitlab.com/utsekaj/starfish`
5. `pip install -e starfish`

Now any changes you make to the code at this location will be reflected when importing or running starfish in this environment


## Easiest test of the installation

### Automated test suite
```bash
cd AutoRunTest
sh get_test_data.sh # Download necessary comparison data
pytest . # Note this will take a long time and depends on pytest
```

### Old tests
If the software environment has been installed correctly, you should be able to navigate to the
folder `UnitTesting` and run any of the python scripts therein e.g. `python unitTest_singleVessel.py`.
If any errors were reported something is wrong.

The typical usage of STARFiSh assumes one has a working directory where the configuration and
results for simulating a specific vascular network may be stored.

The topology, geometry, boundary conditions and simulation parameters are all stored in an XML file
in a folder under the name of the network (typically referring to a topology+geometry) though this is
not enforced.
For each specific set of simulation parameters a simulation number is assigned and a subfolder with
this number will be created where the exact network XML file will be saved along with all simulation
output data.